package co.edu.javeriana.faros.btooth;

import android.bluetooth.BluetoothGatt;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import co.edu.javeriana.faros.RoutinesWeekTabbedActivity;
import co.edu.javeriana.faros.btooth.BluetoothFaroGattService;

public class BluetoothFaroGattReceiver extends BroadcastReceiver {
    private static final String TAG = BluetoothFaroGattReceiver.class.getSimpleName();
    public BluetoothFaroGattReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();
        final String address = intent.getStringExtra(BluetoothFaroGattService.EXTRA_ADDRESS);
        final int status = intent.getIntExtra(BluetoothFaroGattService.EXTRA_STATUS, BluetoothGatt.GATT_FAILURE);

        Log.d(TAG,"onReceive "+address+" "+action+" "+" status:"+status);

        if(context instanceof RoutinesWeekTabbedActivity) {
            RoutinesWeekTabbedActivity mRoutinesWeekTabbedActivity =
                    (RoutinesWeekTabbedActivity) context;

            switch (action) {
                case BluetoothFaroGattService.ACTION_GATT_CONNECTED: {
                    mRoutinesWeekTabbedActivity.faroGattConnected(address, status);
                    break;
                }
                case BluetoothFaroGattService.ACTION_GATT_DISCONNECTED: {
                    mRoutinesWeekTabbedActivity.faroGattDisconnected(address, status);
                    break;
                }
                case BluetoothFaroGattService.ACTION_GATT_SERVICES_DISCOVERED: {
                    mRoutinesWeekTabbedActivity.faroGattServicesDiscovered(address, status);
                    break;
                }
                case BluetoothFaroGattService.ACTION_GATT_CHARACT_READ: {
                    String uuid = intent.getStringExtra(BluetoothFaroGattService.EXTRA_UUID);
                    byte[] value = intent.getByteArrayExtra(BluetoothFaroGattService.EXTRA_DATA);
                    Log.d(TAG,"ACTION_GATT_CHARACT_READ "+address+" "+uuid+" "+" value:"+value);
                    mRoutinesWeekTabbedActivity.faroGattCharacteristicRead(address, status, uuid, value);
                    break;
                }
                case BluetoothFaroGattService.ACTION_GATT_CHARACT_WRITE: {
                    String uuid = intent.getStringExtra(BluetoothFaroGattService.EXTRA_UUID);
                    byte[] value = intent.getByteArrayExtra(BluetoothFaroGattService.EXTRA_DATA);
                    Log.d(TAG,"ACTION_GATT_CHARACT_WRITE "+address+" "+uuid+" "+" value:"+value);
                    mRoutinesWeekTabbedActivity.faroGattCharacteristicWrite(address, status, uuid, value);
                    break;
                }
                case BluetoothFaroGattService.ACTION_GATT_CHARACT_NOTIFY: {
                    String uuid = intent.getStringExtra(BluetoothFaroGattService.EXTRA_UUID);
                    byte[] value = intent.getByteArrayExtra(BluetoothFaroGattService.EXTRA_DATA);
                    int routinePos = intent.getIntExtra(BluetoothFaroGattService.EXTRA_ROUTINE_POS,-1);
                    Log.d(TAG,"ACTION_GATT_CHARACT_NOTIFY "+address+" "+uuid+" "+" value:"+value+" "+" routinePos:"+routinePos);
                    mRoutinesWeekTabbedActivity.faroGattCharacteristicNotify(address, status, uuid, value, routinePos);
                    break;
                }
                case BluetoothFaroGattService.ACTION_GATT_DESCRIPTOR_WRITE: {
                    String uuid = intent.getStringExtra(BluetoothFaroGattService.EXTRA_UUID);
                    byte[] value = intent.getByteArrayExtra(BluetoothFaroGattService.EXTRA_DATA);
                    Log.d(TAG,"ACTION_GATT_DESCRIPTOR_WRITE "+address+" "+uuid+" "+" value:"+value);
                    mRoutinesWeekTabbedActivity.faroGattDescriptorWrite(address, status, uuid, value);
                    break;
                }
                case BluetoothFaroGattService.ACTION_GATT_WRITE_COMMITTED: {
                    mRoutinesWeekTabbedActivity.faroGattWriteCommitted(address, status);
                    break;
                }
            }
        }
    }
}
