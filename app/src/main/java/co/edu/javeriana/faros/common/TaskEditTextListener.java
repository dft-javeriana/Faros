package co.edu.javeriana.faros.common;

import android.text.Editable;
import android.text.TextWatcher;

import co.edu.javeriana.faros.tasks.Task;

/**
 * Created by cesar on 4/8/16.
 */
public class TaskEditTextListener  implements TextWatcher {
    private int mTaskPosition;

    public void setTaskPosition(int taskPosition) {
        mTaskPosition = taskPosition;
    }

    public int getTaskPosition(){
        return mTaskPosition;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        // no op
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        // no op
    }
}