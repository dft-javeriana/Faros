package co.edu.javeriana.faros.btooth;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import co.edu.javeriana.faros.R;
import co.edu.javeriana.faros.RoutinesWeekTabbedActivity;

public class BluetoothAdapterReceiver extends BroadcastReceiver {
    public BluetoothAdapterReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();
        // Actions for Bluetooth TURNING/ON/OFF
        RoutinesWeekTabbedActivity mRoutinesWeekTabbedActivity = null;
        TextView mBluetoothStateTextView = null;
        Button mBluetoothEnableButton = null;
        if(context instanceof RoutinesWeekTabbedActivity){
            mRoutinesWeekTabbedActivity = (RoutinesWeekTabbedActivity) context;
            mBluetoothStateTextView = mRoutinesWeekTabbedActivity.getBluetoothStateTextView();
            mBluetoothEnableButton = mRoutinesWeekTabbedActivity.getBluetoothEnableButton();
        }
        if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
            final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                    BluetoothAdapter.ERROR);
            switch (state) {
                case BluetoothAdapter.STATE_OFF:
                    // Show message text box, show enable button
                    if (mBluetoothStateTextView != null && mBluetoothEnableButton != null) {
                        mBluetoothStateTextView.setVisibility(View.VISIBLE);
                        mBluetoothEnableButton.setVisibility(View.VISIBLE);
                        mBluetoothStateTextView.setText(R.string.bt_off);
                        mBluetoothStateTextView.setBackgroundColor(0xFFF00000);
                        mBluetoothEnableButton.setText(R.string.bt_enable);
                    }
                    break;
                case BluetoothAdapter.STATE_TURNING_OFF:
                    // Do nothing
                    break;
                case BluetoothAdapter.STATE_ON:
                    // Remove message text box and enable button
                    if (mBluetoothStateTextView != null && mBluetoothEnableButton != null) {
                        mBluetoothStateTextView.setVisibility(View.GONE);
                        mBluetoothEnableButton.setVisibility(View.GONE);
                        mBluetoothStateTextView.setText("");
                        mBluetoothStateTextView.setBackgroundColor(0x00000000);
                        mBluetoothEnableButton.setText("");
                    }
                    break;
                case BluetoothAdapter.STATE_TURNING_ON:
                    // Show message text box, remove enable button
                    if (mBluetoothStateTextView != null && mBluetoothEnableButton != null) {
                        mBluetoothStateTextView.setVisibility(View.VISIBLE);
                        mBluetoothEnableButton.setVisibility(View.VISIBLE);
                        mBluetoothStateTextView.setText(R.string.bt_turning_on);
                        mBluetoothStateTextView.setBackgroundColor(0xFF00F000);
                        mBluetoothEnableButton.setText("");
                    }
                    break;
            }
        }
    }
}
