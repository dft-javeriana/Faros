package co.edu.javeriana.faros.btooth;

/**
 * Created by cesar on 3/3/16.
 */
public class BluetoothState {
    private static boolean mBleSupported = false;

    private BluetoothState(){
    }

    public static void setBleSupport(boolean bleSupport){
        mBleSupported = bleSupport;
    }

    public static boolean getBleSupport(){
        return mBleSupported;
    }
}
