package co.edu.javeriana.faros.routines;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

import java.util.Calendar;
import java.util.concurrent.atomic.AtomicInteger;

import co.edu.javeriana.faros.common.AlarmReceiver;
import co.edu.javeriana.faros.common.BootReceiver;

/**
 * Created by cesar on 3/10/16.
 */
public class Routine{
    private int mId = -1;
    private String mName = null;
    private Calendar mWakeupTime =null;
    private boolean mEnable = false;
    private boolean mShowTasks = false;

    private static AtomicInteger mAtomicInteger = null;
    private PendingIntent mAlarmPendingIntent = null;

    public Routine(String name, Calendar wakeupTime, boolean enable, boolean showTasks){
        if(mAtomicInteger == null){
            mAtomicInteger = new AtomicInteger();
        }
        mId = mAtomicInteger.getAndIncrement();
        mName = name;
        mWakeupTime = wakeupTime;
        mEnable = enable;
        mShowTasks = showTasks;
    }

    public Routine(int id, String name, Calendar wakeupTime, boolean enable, boolean showTasks) {
        if(mAtomicInteger == null){
            mAtomicInteger = new AtomicInteger();
        }
        mId = id;
        mName = name;
        mWakeupTime = wakeupTime;
        mEnable = enable;
        mShowTasks = showTasks;
    }

    public Routine(){
        if(mAtomicInteger == null){
            mAtomicInteger = new AtomicInteger();
        }
    }

    public int getId(){
        return mId;
    }

    public void setName(String newName){
        mName = newName;
    }

    public String getName(){
        return mName;
    }

    public void setWakeupTime(Calendar newWakeupTime){
        mWakeupTime = newWakeupTime;
    }

    public Calendar getWakeupTime(){
        return mWakeupTime;
    }

    public void setEnableRoutine(boolean enable){
        mEnable = enable;
    }

    public boolean getEnableRoutine() {
        return mEnable;
    }

    public void setShowTasks(boolean showTasks) { mShowTasks = showTasks; }

    public boolean getShowTasks() { return mShowTasks;}

    public PendingIntent createAlarmPendingIntent(Context context){
        Intent mAlarmIntent = new Intent(context, AlarmReceiver.class);
        return PendingIntent.getBroadcast(context,
                mId, mAlarmIntent, PendingIntent.FLAG_CANCEL_CURRENT);
    }

    // Using the same pending intent for an alarm, replaces the original alarm that was using that
    // pending intent.
    public void setAlarm(Context context){
        // Fire the alarm in weekly intervals from the time set in mWakeupTime
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                mWakeupTime.getTimeInMillis(), AlarmManager.INTERVAL_DAY*7,
                createAlarmPendingIntent(context));

        // Enable {@code BootReceiver} to automatically restart the alarm when the
        // device is rebooted.

        ComponentName bootReceiverName = new ComponentName(context, BootReceiver.class);

        PackageManager pm = context.getPackageManager();
        pm.setComponentEnabledSetting(
                bootReceiverName, PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
    }

    public void disableAlarm(Context context){
        if(mAlarmPendingIntent != null){
            AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
            alarmManager.cancel(mAlarmPendingIntent);
        }
    }

    public PendingIntent getAlarmPendingIntent(){
        return mAlarmPendingIntent;
    }
}