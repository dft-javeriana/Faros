package co.edu.javeriana.faros;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;

public class SplashActivity extends AppCompatActivity {
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private SplashImageTask mSplashTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.activity_splash);

        // Show splash image by setting a sleep in a background task.
        // Then continue with Login/Registration activity
        mSplashTask = new SplashImageTask();
        mSplashTask.execute((Void) null);

    }

    public class SplashImageTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                // Show image before next activity
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success){
            mSplashTask = null;
            // we don't care about success result

            Intent mIntent;
            SharedPreferences credentialsPrefs = getSharedPreferences(RegistrationActivity.CREDENTIALS,
                    Context.MODE_PRIVATE);
            if (credentialsPrefs.contains("user")){
                mIntent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(mIntent);
            }else{
                mIntent = new Intent(SplashActivity.this, RegistrationActivity.class);
                startActivity(mIntent);
            }
        }

        @Override
        protected void onCancelled() {
            mSplashTask = null;
        }
    }
}
