package co.edu.javeriana.faros.btooth;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.Context;
import android.support.annotation.NonNull;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by cesar on 5/12/16.
 */
public class BluetoothRequest implements Comparable<BluetoothRequest>{

    public static final BluetoothGattCharacteristic dummyChar = new BluetoothGattCharacteristic(new UUID(0,0),0,0);

    public enum RequestAction {
        NULL,
        CONNECT,
        DISCONNECT,
        DISCOVER,
        GET_SERVICES,
        READ,
        WRITE,
        WRITE_DESCRIPTOR,
        SET_NOTIFICATION
    }

    public enum RequestStatus {
        NULL,
        ON_QUEUE,
        STARTING,
        STARTED,
        TO,
        DONE,
        FAILED
    }

    private static volatile AtomicInteger mAtomicInteger = null;

    private int id = -1;
    private boolean async = false;
    private boolean statusRead = false;
    private String devAddress = null;
    private BluetoothDevice device = null;
    private RequestAction action = RequestAction.NULL;
    private BluetoothGattCharacteristic characteristic = null;
    private boolean notificationEnabled = false;
    private RequestStatus status = RequestStatus.NULL;
    private int routinePosition = -1;
    private Context context = null;

    public BluetoothRequest(){
        if(mAtomicInteger == null){
            mAtomicInteger = new AtomicInteger();
        }
        id = mAtomicInteger.getAndIncrement();
    }

    public int getId() {
        return id;
    }

    public boolean isAsync() {
        return async;
    }

    public boolean isStatusRead(){return statusRead;}

    public String getDevAddress() {
        return devAddress;
    }

    public BluetoothDevice getDevice() {
        return device;
    }

    public RequestAction getAction(){
        return action;
    }

    public BluetoothGattCharacteristic getCharacteristic() {
        return characteristic;
    }

    public boolean isNotificationEnabled() {
        return notificationEnabled;
    }

    public RequestStatus getStatus() {
        statusRead = true;
        return status;
    }

    public int getRoutinePosition() {
        return routinePosition;
    }

    public Context getContext() {
        return context;
    }


    public BluetoothRequest setAsync(boolean async) {
        this.async = async;
        return this;
    }

    public BluetoothRequest setDevAddress(String devAddress) {
        this.devAddress = devAddress;
        return this;
    }

    public BluetoothRequest setDevice(BluetoothDevice device) {
        this.device = device;
        return this;
    }

    public BluetoothRequest setAction(RequestAction action) {
        this.action = action;
        return this;
    }

    public BluetoothRequest setCharacteristic(BluetoothGattCharacteristic characteristic) {
        this.characteristic = characteristic;
        return this;
    }

    public BluetoothRequest setNotificationEnabled(boolean notificationEnabled) {
        this.notificationEnabled = notificationEnabled;
        return this;
    }

    public BluetoothRequest setStatus(RequestStatus status) {
        this.status = status;
        return this;
    }

    public BluetoothRequest setRoutinePosition(int routinePosition) {
        this.routinePosition = routinePosition;
        return this;
    }

    public BluetoothRequest setContext(Context context) {
        this.context = context;
        return this;
    }

    @Override
    public int compareTo(@NonNull BluetoothRequest another) {
        if(this.devAddress == null || this.characteristic == null
                || another.devAddress == null || another.characteristic == null){
            throw new NullPointerException("Cannot compare object containing null references");
        }

        int compareResult;
        if((compareResult = this.devAddress.compareTo(another.devAddress)) != 0){
            return compareResult;
        }
        if((compareResult = this.action.compareTo(another.action)) != 0){
            return compareResult;
        }
        return this.characteristic.getUuid().compareTo(another.characteristic.getUuid());
    }
}


