package co.edu.javeriana.faros.routines;

import android.app.AlarmManager;
import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import co.edu.javeriana.faros.R;
import co.edu.javeriana.faros.common.CallBack;
import co.edu.javeriana.faros.tasks.Task;
import co.edu.javeriana.faros.tasks.TasksManager;
import co.edu.javeriana.faros.tasks.TasksRecyclerViewAdapter;

import static co.edu.javeriana.faros.routines.RoutinesManager.compareRoutinesDayTime;

/**
 * Created by cesar on 3/9/16.
 */
public class RoutinesRecyclerViewAdapter extends RecyclerView.Adapter<RoutinesRecyclerViewAdapter.ViewHolder> {
    final static String TAG = RoutinesRecyclerViewAdapter.class.getSimpleName();
    final static String deleteRoutineTAG = RoutineDeleteDialogFragment.class.getSimpleName();
    private List<Routine> adapterRoutinesList = null;
    private HashMap<Integer,TasksManager> adapterTasksListCache = new HashMap<Integer,TasksManager>();
    private HashMap<Integer,TasksRecyclerViewAdapter> adapterTasksViewCache = new HashMap<Integer,TasksRecyclerViewAdapter>();

    private Context mContext = null;
    private AlarmManager mAlarmManager = null;
    private CallBack[] mTaskCBs = null;
    private int routineShowingTasks = -1;
    private boolean currentlyBinding  = false;

    public RoutinesRecyclerViewAdapter(Context context,
                                       List<Routine> restoredRoutinesList,
                                       CallBack[] taskCBs){
        mContext = context;
        mAlarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        adapterRoutinesList = restoredRoutinesList;
        mTaskCBs = taskCBs;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView routineStateImgView;
        public TextView alarmTextView;
        public TextView routineNameTextView;
        public RecyclerView tasksRecyclerView;
        public ImageView taskExpandView;
        public ImageView routineBottomView;
        public ImageView routineDeleteView;

        public ViewHolder(View routineItemView){
            super(routineItemView);

            routineStateImgView = (ImageView) routineItemView.findViewById(R.id.routineStateImgView);
            alarmTextView = (TextView) routineItemView.findViewById(R.id.alarmTextView);
            routineNameTextView = (TextView) routineItemView.findViewById(R.id.routineNameTextView);
            tasksRecyclerView = (RecyclerView) routineItemView.findViewById(R.id.tasksRecyclerView);
            taskExpandView = (ImageView) routineItemView.findViewById(R.id.taskExpandView);
            routineBottomView = (ImageView) routineItemView.findViewById(R.id.routineBottomView);
            routineDeleteView = (ImageView) routineItemView.findViewById(R.id.routineDeleteView);

            taskExpandView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!isCurrentlyBinding()) {
                        //TODO: commit changes to routines list
                        int position = getAdapterPosition();
                        Routine editRoutine = adapterRoutinesList.get(position);
                        if (editRoutine.getShowTasks()) {
                            editRoutine.setShowTasks(false);
                            adapterRoutinesList.set(position, editRoutine);
                            routineShowingTasks = -1;
                        } else {
                            editRoutine.setShowTasks(true);
                            adapterRoutinesList.set(position, editRoutine);
                            if (routineShowingTasks != -1) {
                                Routine hideTasksRoutine = adapterRoutinesList.get(routineShowingTasks);
                                hideTasksRoutine.setShowTasks(false);
                                adapterRoutinesList.set(routineShowingTasks, hideTasksRoutine);
                                notifyItemChanged(routineShowingTasks);
                            }
                            routineShowingTasks = position;
                        }
                        notifyItemChanged(position);
                    }
                }
            });


            routineDeleteView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Routine routine  = adapterRoutinesList.get(getAdapterPosition());
                    FragmentManager fragmentManager = ((AppCompatActivity)mContext).getSupportFragmentManager();
                    RoutineDeleteDialogFragment.newInstance(getAdapterPosition(),routine.getName())
                            .show(fragmentManager,deleteRoutineTAG);
                }
            });
        }

        public ImageView getRoutineStateImgView() {
            return routineStateImgView;
        }
        public TextView getAlarmTextView(){
            return alarmTextView;
        }
        public TextView getRoutineNameTextView(){
            return routineNameTextView;
        }
        public RecyclerView getTasksRecyclerView(){
            return tasksRecyclerView;
        }
        public ImageView getTaskExpandView() {
            return taskExpandView;
        }
        public ImageView getRoutineBottomView() {
            return routineBottomView;
        }
        public ImageView getRoutineDeleteView() {
            return routineDeleteView;
        }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RoutinesRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.routine_item_layout, parent, false);

        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RoutinesRecyclerViewAdapter.ViewHolder holder, int position) {
        //Replace contents of view with data from item in adapterRoutinesList at position.
        Routine routine = adapterRoutinesList.get(position);

        if(routine == null){
            holder.getRoutineStateImgView().setImageResource(android.R.color.transparent);
            holder.getAlarmTextView().setText("");
            holder.getRoutineNameTextView().setText("");
            holder.getTasksRecyclerView().setVisibility(View.GONE);
            holder.getTaskExpandView().setImageResource(android.R.color.transparent);
            holder.getRoutineBottomView().setImageResource(android.R.color.transparent);
            holder.getTaskExpandView().setImageResource(android.R.color.transparent);
            holder.getRoutineDeleteView().setImageResource(android.R.color.transparent);
        }else{
            DateFormat dayTimeFormat = new SimpleDateFormat("HH:mm", Locale.US);
            String dayTimeString = dayTimeFormat.format(routine.getWakeupTime().getTime());
            holder.getAlarmTextView().setText(dayTimeString);
            holder.getRoutineNameTextView().setText(routine.getName());
            holder.getRoutineBottomView().setImageResource(android.R.color.white);
            if(routine.getEnableRoutine()) {
                holder.getRoutineStateImgView().setImageResource(R.drawable.check_mark);
            }
            else{
                holder.getRoutineStateImgView().setImageResource(R.drawable.x_mark);
            }

            TasksManager mTasksManager = getTaskManagerFromCache(routine.getId());
            TasksRecyclerViewAdapter mTasksRecyclerViewAdapter
                    = getTaskViewFromCache(mTasksManager,position,routine.getId());
            RecyclerView.LayoutManager mTaskLayoutManager = new LinearLayoutManager(mContext);
            holder.getTasksRecyclerView().setLayoutManager(mTaskLayoutManager);
            holder.getTasksRecyclerView().setAdapter(mTasksRecyclerViewAdapter);
            holder.getTasksRecyclerView().setHasFixedSize(true);
            if(routine.getShowTasks()) {
                holder.getTaskExpandView().setImageResource(R.drawable.ic_expand_less_black_24dp);
                holder.getRoutineDeleteView().setImageResource(android.R.color.transparent);
                holder.getTasksRecyclerView().setVisibility(View.VISIBLE);
            }
            else{
                holder.getTaskExpandView().setImageResource(R.drawable.ic_expand_more_black_24dp);
                holder.getRoutineDeleteView().setImageResource(R.drawable.ic_delete_black_24dp);
                holder.getTasksRecyclerView().setVisibility(View.GONE);
            }
        }
    }

    @Override
    public long getItemId(int position) {
        return adapterRoutinesList.get(position).getId();
    }

    @Override
    public int getItemCount() {
        return adapterRoutinesList.size();
    }

    /**
     * Add routine to the list of the routines adapter, ordered by
     * increasing alarm wakeup hour and then increasing alarm wakeup minute.
     * Notifies the adapter that a new item is at the position inserted.
     * @param newRoutine
     */

    public int addRoutineItem(Routine newRoutine){
        int i;
        for(i = 0; i < adapterRoutinesList.size(); i++){
            Routine routineCmp = adapterRoutinesList.get(i);
            if(compareRoutinesDayTime(newRoutine,routineCmp) < 0){
                break;
            }
        }
        adapterRoutinesList.add(i, newRoutine);
        return i;
    }

    public void deleteRoutineItem(int routinePosition){
        adapterRoutinesList.remove(routinePosition);
    }

    public void deleteTasksForRoutineItem(int routinePosition){
        // delete related tasks from disk, disk cache and view adapter
        TasksManager mTasksManager = getTaskManagerFromCache((int) getItemId(routinePosition));
        mTasksManager.deleteTasksOnDisk(mContext);
        deleteTaskManagerFromCache((int) getItemId(routinePosition));
        deleteTaskViewFromCache((int) getItemId(routinePosition));
    }

    public int getTaskItemCount(int routinePosition){
        TasksManager mTasksManager = getTaskManagerFromCache((int) getItemId(routinePosition));
        TasksRecyclerViewAdapter mTasksRecyclerViewAdapter =
                getTaskViewFromCache(mTasksManager, routinePosition, (int) getItemId(routinePosition));
        return mTasksRecyclerViewAdapter.getItemCount();
    }

    public void setTaskItem(Task newTask, int taskPosition, int routinePosition){
        TasksManager mTasksManager = getTaskManagerFromCache((int) getItemId(routinePosition));
        TasksRecyclerViewAdapter mTasksRecyclerViewAdapter =
                getTaskViewFromCache(mTasksManager, routinePosition, (int) getItemId(routinePosition));

        if(taskPosition == mTasksRecyclerViewAdapter.getItemCount()-1){
            mTasksRecyclerViewAdapter.setDummyItem(newTask);
        }
        else {
            mTasksManager.addTaskToCache(newTask);
            try {
                mTasksManager.commitTasksToDisk(mContext);
            } catch (IOException e) {
                e.printStackTrace();
            }
            mTasksRecyclerViewAdapter.setItem(newTask, taskPosition);
        }
    }

    public void addTaskItem(Task newTask, int routinePosition){
        TasksManager mTasksManager = getTaskManagerFromCache((int) getItemId(routinePosition));
        TasksRecyclerViewAdapter mTasksRecyclerViewAdapter =
                getTaskViewFromCache(mTasksManager, routinePosition, (int) getItemId(routinePosition));

        mTasksManager.addTaskToCache(newTask);
        try {
            mTasksManager.commitTasksToDisk(mContext);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mTasksRecyclerViewAdapter.addItem(newTask);
    }

    public Task getTaskItem(int taskPosition, int routinePosition){
        TasksManager mTasksManager = getTaskManagerFromCache((int) getItemId(routinePosition));
        TasksRecyclerViewAdapter mTasksRecyclerViewAdapter =
                getTaskViewFromCache(mTasksManager,routinePosition,(int) getItemId(routinePosition));

        return mTasksRecyclerViewAdapter.getItem(taskPosition);
    }

    public void notifyTaskItemChanged(int taskPosition, int routinePosition){
        TasksManager mTasksManager = getTaskManagerFromCache((int) getItemId(routinePosition));
        TasksRecyclerViewAdapter mTasksRecyclerViewAdapter =
                getTaskViewFromCache(mTasksManager,routinePosition,(int) getItemId(routinePosition));
        mTasksRecyclerViewAdapter.notifyItemChanged(taskPosition);
    }

    public void notifyTaskItemInserted(int taskPosition, int routinePosition){
        TasksManager mTasksManager = getTaskManagerFromCache((int) getItemId(routinePosition));
        TasksRecyclerViewAdapter mTasksRecyclerViewAdapter =
                getTaskViewFromCache(mTasksManager,routinePosition,(int) getItemId(routinePosition));
        mTasksRecyclerViewAdapter.notifyItemInserted(taskPosition);
    }

    public void resetTaskDummy(int routinePosition){
        TasksManager mTasksManager = getTaskManagerFromCache((int) getItemId(routinePosition));
        TasksRecyclerViewAdapter mTasksRecyclerViewAdapter =
                getTaskViewFromCache(mTasksManager,routinePosition,(int) getItemId(routinePosition));
        mTasksRecyclerViewAdapter.resetDummy();
    }

    public boolean isCurrentlyBinding() {
        return currentlyBinding;
    }

    public void setCurrentlyBinding(boolean currentlyBinding) {
        this.currentlyBinding = currentlyBinding;
    }

    private TasksManager getTaskManagerFromCache(int routineId){
        TasksManager mTasksManager = adapterTasksListCache.get(routineId);
        if(mTasksManager == null){
            mTasksManager = new TasksManager(routineId);
            mTasksManager.readTasksFromDisk(mContext);
            adapterTasksListCache.put(routineId,mTasksManager);
        }
        return mTasksManager;
    }

    private TasksRecyclerViewAdapter getTaskViewFromCache(
            TasksManager tasksManager, int routinePosition, int routineId){
        TasksRecyclerViewAdapter mTasksRecyclerViewAdapter = adapterTasksViewCache.get(routineId);
        if(mTasksRecyclerViewAdapter == null) {
            mTasksRecyclerViewAdapter = new TasksRecyclerViewAdapter(
                    mContext,
                    routinePosition,
                    tasksManager.getTasksListFromCache(),
                    mTaskCBs);
            adapterTasksViewCache.put(routineId,mTasksRecyclerViewAdapter);
        }
        return mTasksRecyclerViewAdapter;
    }

    private void deleteTaskViewFromCache(int routineId) {
        adapterTasksViewCache.remove(routineId);
    }

    private void deleteTaskManagerFromCache(int routineId) {
        adapterTasksListCache.remove(routineId);
    }
}
