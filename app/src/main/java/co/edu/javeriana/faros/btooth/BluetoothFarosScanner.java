package co.edu.javeriana.faros.btooth;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

import co.edu.javeriana.faros.common.CallBack;

/**
 * Created by cesar on 4/12/16.
 */
public class BluetoothFarosScanner {
    private static final int SCAN_TIME_MILLIS = 0;

    private static final String TAG = "BluetoothFarosScanner";
    public static final String ARG_FARO_MAC = "FARO_MAC";
    public static final String ARG_FARO_ROUTINE = "FARO_ROUTINE";

    @SuppressWarnings("unchecked") // to avoid Comparable<Comparable<Comparable<...>>>
    private static final Comparator<Comparable> INVERSE_NATURAL_ORDER = new Comparator<Comparable>() {
        public int compare(Comparable a, Comparable b) {
            return b.compareTo(a);
        }
    };

    private static boolean mBleScanning = false;

    private static CallBack<String, String> mBleFarosScanCallBack = null;

    private static BluetoothAdapter mBluetoothAdapter = null;

    static int mRoutinePosition = -1;
    private static BluetoothAdapter.LeScanCallback mLeScanCallback = null;
    private static String[] mDeviceNameFilterArray;

    private static ScanSettings mScanSettings = null;
    private static ScanCallback mScanCallback = null;
    private static List<ScanFilter> mScanFilters = null;

    // List of faros sorted by their ascending rssi using a red-black tree
    // It may contain repeated MACs but we don't care since we only
    // need the one device with the strongest rssi.
    private static TreeMap<Integer,String> farosMACsRssiAscSorted
            = new TreeMap<Integer,String>(INVERSE_NATURAL_ORDER);
    private static TreeMap<String,Integer> farosLastRssiMACSorted
            = new TreeMap<String,Integer>();
    public static void BluetoothScanner(BluetoothAdapter bluetoothAdapter,
                                        String[] deviceNameFilterArray){
        mBluetoothAdapter = bluetoothAdapter;
        mDeviceNameFilterArray = deviceNameFilterArray;
    }

    public static void scanInit(){
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            scanInitAPI23();
        } else if (android.os.Build.VERSION.SDK_INT >= 21) {
            scanInitAPI21(); //TODO: Not tested
        } else{
            scanInitAPI18();
        }
    }

    private static void scanInitAPI18(){
        // Callback only provides one scan result per scan
        mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
            public void onLeScan(final BluetoothDevice device, final int rssi, final byte[] scanRecord) {
                String bleDeviceName = device.getName();
                if (bleDeviceName == null) {
                    ParseScanRecord mParseScanRecord;
                    mParseScanRecord = new ParseScanRecord(scanRecord);
                    bleDeviceName = mParseScanRecord.getName();
                }

                // Filter devices for faros
                if (checkDeviceFilter(bleDeviceName)) {
                    updateFaroRssi(rssi,device.getAddress());
                }
            }
        };
    }

    @TargetApi(21)
    private static void scanInitAPI21(){
        // Initialize device filter
        mScanFilters = new ArrayList<>();
        for (String deviceNameFilter : mDeviceNameFilterArray) {
            ScanFilter mScanFilter = new ScanFilter.Builder()
                    .setDeviceName(deviceNameFilter)
                    .build();
            mScanFilters.add(mScanFilter);
        }

        // Initialize scan settings
        mScanSettings = new ScanSettings.Builder()
                // Delay of report in milliseconds. Set to 0 to be notified of results
                // immediately. Values > 0 causes the scan results to be queued up and
                // delivered after the requested delay or when the internal buffers fill up.
                .setReportDelay(SCAN_TIME_MILLIS)

                        // Perform Bluetooth LE scan in balanced power mode.
                .setScanMode(ScanSettings.SCAN_MODE_BALANCED)
                .build();

        mScanCallback = new ScanCallback() {
            @Override
            public void onBatchScanResults(final List<ScanResult> results) {
                super.onBatchScanResults(results);
                for(ScanResult result : results){

                    updateFaroRssi(result.getRssi(), result.getDevice().getAddress());

                    Log.i(TAG, "scanInitAPI21 onBatchScanResults " + result.getDevice().getName() + " " + result.getDevice().getAddress());
                }
                sendStrongestFaro();
            }

            @Override
            public void onScanResult(final int callbackType, ScanResult result) {
                super.onScanResult(callbackType, result);

                updateFaroRssi(result.getRssi(), result.getDevice().getAddress());

                Log.i(TAG, "scanInitAPI21 onScanResult  " + result.getDevice().getName() + " " + result.getDevice().getAddress());

                sendStrongestFaro();
            }

            @Override
            public void onScanFailed(int errorCode) {
                super.onScanFailed(errorCode);
                switch (errorCode) {
                    case SCAN_FAILED_ALREADY_STARTED:
                        Log.e(TAG, "scanInitAPI21 onScanFailed Scan failed, already started");
                        break;
                    case SCAN_FAILED_APPLICATION_REGISTRATION_FAILED:
                        Log.e(TAG, "scanInitAPI21 onScanFailed Scan failed, application registration failed");
                        break;
                    case SCAN_FAILED_FEATURE_UNSUPPORTED:
                        Log.e(TAG, "scanInitAPI21 onScanFailed Scan failed, power optimized scan not supported ");
                        break;
                    case SCAN_FAILED_INTERNAL_ERROR:
                        Log.e(TAG, "scanInitAPI21 onScanFailed Scan failed, internal error");
                        break;
                }
            }
        };
    }

    @TargetApi(23)
    private static void scanInitAPI23() {
        // Initialize device filter
        mScanFilters = new ArrayList<>();
        for (String deviceNameFilter : mDeviceNameFilterArray) {
            ScanFilter mScanFilter = new ScanFilter.Builder()
                    .setDeviceName(deviceNameFilter)
                    .build();
            mScanFilters.add(mScanFilter);
        }

        // Initialize scan settings
        mScanSettings = new ScanSettings.Builder()
                // Trigger a callback for every Bluetooth advertisement
                // found that matches the filter criteria.
               .setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES)

                        // In Aggressive mode, hw will determine a match sooner even with feeble
                        // signal strength and few number of sightings/match in a duration.
                .setMatchMode(ScanSettings.MATCH_MODE_AGGRESSIVE)

                        // Match as many advertisement per filter as hw could allow, depends on
                        // current capability and availability of the resources in hw
                .setNumOfMatches(ScanSettings.MATCH_NUM_MAX_ADVERTISEMENT)

                        // Delay of report in milliseconds. Set to 0 to be notified of results
                        // immediately. Values > 0 causes the scan results to be queued up and
                        // delivered after the requested delay or when the internal buffers fill up.
                .setReportDelay(SCAN_TIME_MILLIS)

                        // Perform Bluetooth LE scan in balanced power mode.
                .setScanMode(ScanSettings.SCAN_MODE_BALANCED)
                .build();

        mScanCallback = new ScanCallback() {
            @Override
            public void onBatchScanResults(final List<ScanResult> results) {
                super.onBatchScanResults(results);
                for(ScanResult result : results){

                    updateFaroRssi(result.getRssi(), result.getDevice().getAddress());

                    Log.i(TAG, "scanInitAPI23 onBatchScanResults " + result.getDevice().getName() + " " + result.getDevice().getAddress());
                }
                sendStrongestFaro();
            }

            @Override
            public void onScanResult(final int callbackType, ScanResult result) {
                super.onScanResult(callbackType, result);

                if (callbackType == ScanSettings.CALLBACK_TYPE_ALL_MATCHES) {

                    updateFaroRssi(result.getRssi(), result.getDevice().getAddress());

                    Log.i(TAG, "scanInitAPI23 onScanResult " + result.getDevice().getName() + " " + result.getDevice().getAddress());
                }
                sendStrongestFaro();
            }

            @Override
            public void onScanFailed(int errorCode) {
                super.onScanFailed(errorCode);
                switch (errorCode) {
                    case SCAN_FAILED_ALREADY_STARTED:
                        Log.e(TAG, "scanInitAPI23 onScanFailed Scan failed, already started");
                        break;
                    case SCAN_FAILED_APPLICATION_REGISTRATION_FAILED:
                        Log.e(TAG, "scanInitAPI23 onScanFailed Scan failed, application registration failed");
                        break;
                    case SCAN_FAILED_FEATURE_UNSUPPORTED:
                        Log.e(TAG, "scanInitAPI23 onScanFailed Scan failed, power optimized scan not supported ");
                        break;
                    case SCAN_FAILED_INTERNAL_ERROR:
                        Log.e(TAG, "scanInitAPI23 onScanFailed Scan failed, internal error");
                        break;
                }
            }
        };

    }

    public static void startScan(CallBack<String, String> bleFarosScanCallBack, int routinePosition){
        if(mBleScanning){
            return;
        }
        mBleScanning = true;
        mRoutinePosition = routinePosition;
        mBleFarosScanCallBack = bleFarosScanCallBack;
        // Start device discovery
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            if(mScanCallback == null || mScanFilters == null || mScanSettings == null){
                scanInit();
            }
            startScanAPI21();
        }else{
            if(!startScanAPI18()){
                Log.e(TAG, "startScan Scan failed");
            }else{
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        sendStrongestFaro();
                    }
                }, SCAN_TIME_MILLIS);
            }
        }
    }

    private static boolean startScanAPI18(){
        return mBluetoothAdapter.startLeScan(mLeScanCallback);
    }

    @TargetApi(21)
    private static void startScanAPI21(){
        BluetoothLeScanner mBluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
        mBluetoothLeScanner.startScan(mScanFilters, mScanSettings, mScanCallback);
    }

    public static void stopScan(){
        mBleScanning = false;
        // Start device discovery
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            stopScanAPI21();
        }else{
            stopScanAPI18();
        }
    }

    private static void stopScanAPI18(){
        mBluetoothAdapter.stopLeScan(mLeScanCallback);
    }

    @TargetApi(21)
    private static void stopScanAPI21(){
        BluetoothLeScanner mBluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
        mBluetoothLeScanner.stopScan(mScanCallback);
    }

    private static void sendStrongestFaro(){
        Log.d(TAG, "sendStrongestFaro");
        stopScan();
        String faroMac = farosMACsRssiAscSorted.firstEntry().getValue();
        HashMap<String,String> callBackData = new HashMap<String,String>();
        callBackData.put(ARG_FARO_MAC,faroMac);
        callBackData.put(ARG_FARO_ROUTINE,Integer.toString(mRoutinePosition));
        mBleFarosScanCallBack.callBackMethod(callBackData);
    }

    private static void updateFaroRssi(int rssi, String faroAddress){
        Integer lastRssi = farosLastRssiMACSorted.get(faroAddress);
        if(lastRssi != null){
            // we remove previous MAC to prevent duplicate MACs
            farosMACsRssiAscSorted.remove(lastRssi);
        }
        farosLastRssiMACSorted.put(faroAddress,rssi);
        farosMACsRssiAscSorted.put(rssi, faroAddress);

    }

    private static boolean checkDeviceFilter(String deviceName) {
        // Allow all devices if the device filter is empty
        if(mDeviceNameFilterArray.length == 0){
            return true;
        }
        if (deviceName == null) {
            return false;
        }
        for(String deviceNameFilter : mDeviceNameFilterArray){
            if(deviceName.equals(deviceNameFilter)){
                return true;
            }
        }
        return false;
    }
}
