package co.edu.javeriana.faros.common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.List;

import co.edu.javeriana.faros.routines.Routine;
import co.edu.javeriana.faros.routines.RoutinesManager;

/**
 * Created by cesar on 3/29/16.
 */
public class BootReceiver extends BroadcastReceiver{

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED"))
        {
            RoutinesManager.readRoutinesFromDisk(context);
            List<Routine> routinesList = RoutinesManager.getRoutinesListFromCache();
            for(Routine routine : routinesList){
                if(routine.getEnableRoutine()){
                    routine.setAlarm(context);
                }
            }
        }
    }
}
