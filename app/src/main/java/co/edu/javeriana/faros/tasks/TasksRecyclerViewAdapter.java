package co.edu.javeriana.faros.tasks;

import android.app.AlarmManager;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import co.edu.javeriana.faros.R;
import co.edu.javeriana.faros.RoutinesWeekTabbedActivity;
import co.edu.javeriana.faros.common.CallBack;
import co.edu.javeriana.faros.common.TaskEditTextListener;

/**
 * Created by cesar on 3/9/16.
 */
public class TasksRecyclerViewAdapter extends RecyclerView.Adapter<TasksRecyclerViewAdapter.ViewHolder> {
    private static final String TAG = "TasksViewAdapter";
    private List<Task> adapterTasksList = null;
    private Task mDummyTask = new Task(-2,"","",null);
    private boolean mDummyTaskEditing = false;
    private int taskOnFocus = -1;

    private Context mContext = null;
    private int mRoutinePosition = -1;
    private AlarmManager mAlarmManager = null;
    private CallBack<String, Integer> mRequestImageCaptureCB = null;
    private CallBack<String, String> mTaskNameUpdateCB = null;
    private CallBack<String, Integer> mStartBindingFaroCB = null;

    public TasksRecyclerViewAdapter(Context context,
                                    int routinePosition,
                                    List<Task> restoredTasksList,
                                    CallBack[] taskCBs){
        mContext = context;
        mRoutinePosition = routinePosition;
        mAlarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        adapterTasksList = restoredTasksList;
        mRequestImageCaptureCB = taskCBs[0];
        mTaskNameUpdateCB = taskCBs[1];
        mStartBindingFaroCB = taskCBs[2];
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView taskPicImgView;
        public ImageView taskPicBoxImgView;
        public ImageView bleDeviceImgView;
        public ImageView taskNameBoxImgView;
        public TextView bleDeviceStateTextView;
        public EditText taskNameEditText;
        public MainTaskEditTextListener taskNameEditTextListener;

        public ViewHolder(View taskItemView){
            super(taskItemView);
            taskPicImgView = (ImageView) taskItemView.findViewById(R.id.taskPicImgView);
            taskPicBoxImgView = (ImageView) taskItemView.findViewById(R.id.taskPicBoxImgView);
            bleDeviceImgView = (ImageView) taskItemView.findViewById(R.id.bleDeviceImgView);
            taskNameBoxImgView = (ImageView) taskItemView.findViewById(R.id.taskNameBoxImgView);
            taskNameEditText = (EditText) taskItemView.findViewById(R.id.taskNameEditText);
            bleDeviceStateTextView = (TextView) taskItemView.findViewById(R.id.bleDeviceStateTextView);

            taskPicImgView.setOnClickListener(this);
            bleDeviceImgView.setOnClickListener(this);
            taskNameEditText.setOnClickListener(this);

            taskNameEditTextListener = new MainTaskEditTextListener();
            taskNameEditText.addTextChangedListener(taskNameEditTextListener);
        }

        @Override
        public void onClick(View v) {
            HashMap<String,Integer> callBackData = new HashMap<String,Integer>();
            switch(v.getId()){
                case R.id.taskPicImgView:
                    callBackData.put(RoutinesWeekTabbedActivity.WeekSectionFragment.ARG_ROUTINE_POS,mRoutinePosition);
                    callBackData.put(RoutinesWeekTabbedActivity.WeekSectionFragment.ARG_TASK_POS, getAdapterPosition());
                    mRequestImageCaptureCB.callBackMethod(callBackData);
                    taskOnFocus = getAdapterPosition();
                    if(taskOnFocus == adapterTasksList.size()){
                        mDummyTaskEditing = true;
                    }
                    break;
                case R.id.bleDeviceImgView:
                    if(getAdapterPosition() == getItemCount()-1){
                        Log.d(TAG,"onClick BLE, dummy task");
                        callBackData.put(RoutinesWeekTabbedActivity.WeekSectionFragment.ARG_ROUTINE_POS,mRoutinePosition);
                        mStartBindingFaroCB.callBackMethod(callBackData);
                    }
//                    addItem(mDummyTask);
//                    mDummyTask.setName("");
//                    mDummyTask.setBleAddress("");
//                    mDummyTask.setPicUriFromFile(null);
//                    mDummyTask.setId(-2);
//                    mDummyTaskEditing = false;
//                    taskOnFocus = -1;
                    break;
                case R.id.taskNameEditText:
                    taskOnFocus = getAdapterPosition();
                    if(taskOnFocus == adapterTasksList.size()){
                        mDummyTaskEditing = true;
                    }
                    break;
                default:
                    break;
            }
            // task view focus changes, we must bind again for focus
            // transparency effect with notifyDataSetChanged();
            // Calling this forces any observers to assume that all existing
            // items and structure may no longer be valid. Hence we call it
            // at end of onClick, a method where we need observer information on items.
            notifyDataSetChanged();
        }

        public ImageView getTaskPicImgView(){
            return taskPicImgView;
        }
        public ImageView getTaskPicBoxImgView(){
            return taskPicBoxImgView;
        }
        public ImageView getBleDeviceImgView(){
            return bleDeviceImgView;
        }
        public ImageView getTaskNameBoxImgView(){
            return taskNameBoxImgView;
        }
        public EditText getTaskNameEditText(){
            return taskNameEditText;
        }
        public TextView getBleDeviceStateTextView(){
            return bleDeviceStateTextView;
        }
        public MainTaskEditTextListener getTaskNameEditTextListener(){
            return taskNameEditTextListener;
        }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public TasksRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.task_item_layout, parent, false);
        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(TasksRecyclerViewAdapter.ViewHolder holder, int position) {
        Task task = null;
        //Replace contents of view with data from item in adapterTasksList at position.
        if(position < adapterTasksList.size()) {
            task = adapterTasksList.get(position);
            holder.getTaskNameEditTextListener().setTaskPosition(position);
        }else if(position == adapterTasksList.size()){
            // task item not in list; this is the position of the dummyTask item
            task = mDummyTask;
            holder.getTaskNameEditTextListener().setTaskPosition(position);
        }

        if(task == null){
            holder.getTaskPicImgView().setImageResource(android.R.color.transparent);
            holder.getTaskPicBoxImgView().setImageResource(android.R.color.transparent);
            holder.getBleDeviceImgView().setImageResource(android.R.color.transparent);
            holder.getTaskNameBoxImgView().setImageResource(android.R.color.transparent);
            holder.getTaskNameEditText().setText("");
            holder.getBleDeviceStateTextView().setText("");
        }else {
            holder.getTaskPicBoxImgView().setImageResource(R.drawable.red_pic_box);
            holder.getBleDeviceImgView().setImageResource(R.drawable.red_ble_outline);
            holder.getTaskNameBoxImgView().setImageResource(R.drawable.red_text_box);
            holder.getTaskNameEditText().setText(task.getName());
            if(position == adapterTasksList.size()) {
                if (task.getBleAddress().isEmpty()) {
                    holder.getBleDeviceStateTextView().setText("Not binded");
                } else {
                    holder.getBleDeviceStateTextView().setText("Binded");
                }
            }else{
                holder.getBleDeviceStateTextView().setText("OK");
            }
            //TODO: holder.getBleDeviceStateTextView().setText(bleDevices.get(task.getBleAddress()).getStateString());
            if(position == adapterTasksList.size() && !mDummyTaskEditing){
                holder.getTaskPicImgView().setImageResource(android.R.color.transparent);
            }else{
                holder.getTaskPicImgView().setImageURI(task.getPicUri());
            }
//            if (taskOnFocus != position && taskOnFocus != -1) {
//                holder.getTaskPicImgView().setColorFilter(0x90FFFFFF);
//                holder.getTaskPicBoxImgView().setColorFilter(0x90FFFFFF);
//                holder.getBleDeviceImgView().setColorFilter(0x90FFFFFF);
//                holder.getTaskNameBoxImgView().setColorFilter(0x90FFFFFF);
//                holder.getTaskNameEditText().setTextColor(0x90EE1D3D);
//                holder.getTaskNameEditText().setHintTextColor(0x90EE1D3D);
//                holder.getBleDeviceStateTextView().setTextColor(0x90EE1D3D);
//            }else{
//                holder.getTaskPicImgView().setColorFilter(0x00FFFFFF);
//                holder.getTaskPicBoxImgView().setColorFilter(0x00FFFFFF);
//                holder.getBleDeviceImgView().setColorFilter(0x00FFFFFF);
//                holder.getTaskNameBoxImgView().setColorFilter(0x00FFFFFF);
//                holder.getTaskNameEditText().setTextColor(0xFFEE1D3D);
//                holder.getTaskNameEditText().setHintTextColor(0xFFEE1D3D);
//                holder.getBleDeviceStateTextView().setTextColor(0xFFEE1D3D);
          //  }
        }
    }

    @Override
    public long getItemId(int position) {
        if(position == adapterTasksList.size()){
            return -2; // id for the dummy task
        }
        return adapterTasksList.get(position).getId();
    }

    @Override
    public int getItemCount() {
        // We count an extra item, this being the dummy item
        return adapterTasksList.size()+1;
    }

    /**
     * Add task to the end of the list of the tasks adapter
     * @param newTask
     */
    public void addItem(Task newTask){
        adapterTasksList.add(newTask);
    }

    /**
     * Add task at position of the list of the tasks adapter
     * @param newTask
     * @param position
     */
    public void setItem(Task newTask, int position){
        if(position == adapterTasksList.size()){
            setDummyItem(newTask);
        }
        adapterTasksList.set(position, newTask);
    }

    public Task getItem(int position){
        if(position == adapterTasksList.size()){
            return getDummyItem();
        }
        return adapterTasksList.get(position);
    }

    public void setDummyItem(Task newTask){mDummyTask = newTask;}

    public Task getDummyItem(){return mDummyTask;}

    public void resetDummy(){
        mDummyTask.setName("");
        mDummyTask.setBleAddress("");
        mDummyTask.setPicUriFromFile(null);
        mDummyTask.setId(-2);
        mDummyTaskEditing = false;
        taskOnFocus = -1;
    }

    public void setRoutinePosition(int position){
        mRoutinePosition = position;
    }

    private class MainTaskEditTextListener extends TaskEditTextListener{
        @Override
        public void setTaskPosition(int taskPosition) {
            super.setTaskPosition(taskPosition);
        }
        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            super.onTextChanged(charSequence,i,i2,i3);
            int taskPosition = getTaskPosition();
            HashMap<String, String> data = new HashMap<String, String>();
            data.put(RoutinesWeekTabbedActivity.WeekSectionFragment.ARG_ROUTINE_POS,Integer.toString(mRoutinePosition));
            data.put(RoutinesWeekTabbedActivity.WeekSectionFragment.ARG_TASK_POS,Integer.toString(taskPosition));
            data.put(RoutinesWeekTabbedActivity.WeekSectionFragment.ARG_TASK_NAME,charSequence.toString());
            mTaskNameUpdateCB.callBackMethod(data);
        }
    }
}
