package co.edu.javeriana.faros.routines;

import android.content.Context;
import android.util.JsonReader;
import android.util.JsonWriter;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

/**
 * Created by cesar on 3/29/16.
 *
 * Not thread safe.
 * If necessary, thread safety can be implemented with a semaphore on
 */
public class RoutinesManager {
    /**
     * JSON keys for the routines
     */
    public static final String ROUTINE_JSON_KEY_ID = "id";
    public static final String ROUTINE_JSON_KEY_NAME = "name";
    public static final String ROUTINE_JSON_KEY_ALARM = "alarm";
    public static final String ROUTINE_JSON_KEY_ENABLE = "enable";
    public static final String ROUTINE_JSON_KEY_SHOWTASKS = "showTasks";

    public static final long MINUTE_IN_MILLIS = 1000*60;
    public static final long HOUR_IN_MILLIS = MINUTE_IN_MILLIS*60;
    public static final long DAY_IN_MILLIS = HOUR_IN_MILLIS*24;
    public static final long WEEK_IN_MILLIS = DAY_IN_MILLIS*7;

    private static final String ROUTINES_META_FILE = "routines_meta";
    private static HashMap<Integer,Routine> routinesCache = new HashMap<Integer,Routine>();

    /**
     * <p>Returns cached routines (stored on memory) as a list .
     */
    public static List<Routine> getRoutinesListFromCache(){
        return new ArrayList<Routine>(routinesCache.values());
    }

    /**
     * <p>Returns cached routines (stored on memory) as a list of sorted routines
     * by alarm day time for a selected week day.
     */
    public static List<Routine> getRoutinesListFromCacheWeekViewSorted(int weekDay){
        List<Routine> sortedRoutines = new ArrayList<Routine>();
        for(Routine routine : routinesCache.values()) {
            if(routine.getWakeupTime().get(Calendar.DAY_OF_WEEK) == weekDay) {
                int i;
                for(i = 0; i < sortedRoutines.size(); i++){
                    Routine routineCmp = sortedRoutines.get(i);
                    if(compareRoutinesDayTime(routine,routineCmp) < 0){
                        break;
                    }
                }
                sortedRoutines.add(i,routine);
            }
        }
        return sortedRoutines;
    }

    /**
     * <p>Adds routine to cached routines map.
     */
    public static void addRoutineToCache(Routine newRoutine) {
        routinesCache.put(newRoutine.getId(), newRoutine);
    }

    /**
     * <p>Deletes routine from cache.
     */
    public static void deleteRoutineFromCache(int id){
        routinesCache.remove(id);
    }

    /**
     * <p>Enables routine on cache, also sets pendingIntent on routine.
     */
    public static void enableRoutine(int id){
        Routine routine = null;
        routine = routinesCache.get(id);
        routine.setEnableRoutine(true);
        routinesCache.put(id, routine);
    }

    /**
     * <p>Disables routine on cache,  also cancels pendingIntent on routine.
     */
    public static void disableRoutine(int id){
        Routine routine = null;
        routine = routinesCache.get(id);
        routine.setEnableRoutine(false);
        routinesCache.put(id,routine);
    }

    /**
     * <p>Sets time for routine's alarm on cache. Also modifies alarm on alarmmanager for routine.
     */
    public static void setRoutineAlarm(int id, Calendar wakeupTime){
        Routine routine = null;
        routine = routinesCache.get(id);
        routine.setWakeupTime(wakeupTime);
        routinesCache.put(id, routine);
    }

    /**
     * <p>Changes name for routine on cache.
     */
    public static void nameRoutine(int id, String name){
        Routine routine = null;
        routine = routinesCache.get(id);
        routine.setName(name);
        routinesCache.put(id, routine);
    }

    public static long compareRoutinesDayTime(Routine r1, Routine r2){
        Calendar r1Time = r1.getWakeupTime();
        Calendar r2Time = r2.getWakeupTime();
        long r1TimeGMTOffset = r1Time.get(Calendar.ZONE_OFFSET) + r1Time.get(Calendar.DST_OFFSET);
        long r2TimeGMTOffset = r2Time.get(Calendar.ZONE_OFFSET) + r2Time.get(Calendar.DST_OFFSET);
        return ((r1Time.getTimeInMillis() + r1TimeGMTOffset) % DAY_IN_MILLIS)
                - ((r2Time.getTimeInMillis() + r2TimeGMTOffset) % DAY_IN_MILLIS);
    }

    /**
     * <p>Commits changes from cache on disk.
     * <p>Creates a backup file in case there is a failure while writing to file.
     */

    public static void commitRoutinesToDisk(Context context) throws IOException{
        OutputStream routinesMetaOutStream = null;
        String[] internalFilesList = null;
        internalFilesList = context.fileList();

       /* for(String internalFile : internalFilesList){
            if(internalFile.equals(ROUTINES_META_FILE+".back")){
                throw new IOException("A backup file already exists.");
            }
        }

        try{
            backupInternalFile (ROUTINES_META_FILE,context); //throws IOException
        } catch (IOException e){
            e.printStackTrace();
            if(!context.deleteFile(ROUTINES_META_FILE+".back")){
                throw new IOException("Could not delete backup file.");
            }
        }*/

        try {
            routinesMetaOutStream = new BufferedOutputStream(
                    context.openFileOutput(ROUTINES_META_FILE, Context.MODE_PRIVATE));
        } catch (IOException e) {
            e.printStackTrace();
            try {
                if (routinesMetaOutStream != null) {
                    routinesMetaOutStream.close();
                }
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return;
        }

        try {
            writeRoutinesToJsonStream(routinesMetaOutStream);
        } catch (IOException e) {
            e.printStackTrace();
            throw new IOException("Could not write cache to disk. Attempted to restore from backup");
        }
/*
        if(!context.deleteFile(ROUTINES_META_FILE+".back")){
            throw new IOException("Could not delete backup file.");
        }
*/
    }

    /**
     * <p>Reads the list of routines from disk and copies it to the routines cache
     * that is on memory. If there are changes made to the routines cache and
     * not committed, these will be reset to the values on last commit. Thus an
     * effort must be made to commit after changes are made and before the user
     * exits from the application or changes the context to maintain consistency.
     *
     * <p>Also, changes to related data must be made between changes made to
     * routines on cache and the commit to minimize race conditions.
     *
     * @param context Context that will lock the file from which the
     *                        routinesList is read.
    */
    public static void readRoutinesFromDisk(Context context){
        InputStream routinesMetaInStream = null;

        try {
            routinesMetaInStream = new BufferedInputStream(context.openFileInput(ROUTINES_META_FILE));
            readRoutinesFromJsonStream(routinesMetaInStream);
        } catch (IOException e) {
            e.printStackTrace();
            // There is no data in disk or cache may have been corrupted while reading.
            // Thus, we clear the cache.
            routinesCache.clear();
            if(routinesMetaInStream != null) {
                try {
                    routinesMetaInStream.close();
                }catch (IOException e1) {
                    e.printStackTrace();
                }
            }
            return;
        }

        try {
            if(routinesMetaInStream != null) {
                routinesMetaInStream.close();
            }
        }catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void readRoutinesFromJsonStream(InputStream in) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
        try {
            readRoutinesArray(reader);
        }
        finally{
            reader.close();
        }
    }

    private static void readRoutinesArray(JsonReader reader) throws IOException {
        Routine restoredRoutine;
        reader.beginArray();
        while (reader.hasNext()) {
            restoredRoutine = getJsonRoutine(reader);
            routinesCache.put(restoredRoutine.getId(), restoredRoutine);
        }
        reader.endArray();
    }

    private static Routine getJsonRoutine(JsonReader reader) throws IOException {
        Routine restoredRoutine = null;

        int id = -1;
        String routineName = null;
        Calendar wakeupTime = Calendar.getInstance();
        boolean enable = true;
        boolean showTasks = false;

        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals(ROUTINE_JSON_KEY_ID)) {
                id = reader.nextInt();
            } else if (name.equals(ROUTINE_JSON_KEY_NAME)) {
                routineName = reader.nextString();
            } else if (name.equals(ROUTINE_JSON_KEY_ALARM)) {
                wakeupTime.setTimeInMillis(reader.nextLong());
            } else if (name.equals(ROUTINE_JSON_KEY_ENABLE)) {
                enable = reader.nextBoolean();
            } else if (name.equals(ROUTINE_JSON_KEY_SHOWTASKS)){
                showTasks = reader.nextBoolean();
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        restoredRoutine = new Routine(id, routineName, wakeupTime, enable, showTasks);
        return restoredRoutine;
    }

    public static void writeRoutinesToJsonStream(OutputStream out) throws IOException {
        JsonWriter writer = new JsonWriter(new OutputStreamWriter(out, "UTF-8"));
        writer.setIndent("  ");
        try {
            writeMessagesArray(writer);
        }
        finally {
            writer.close();
        }
    }

    public static void writeMessagesArray(JsonWriter writer) throws IOException {
        writer.beginArray();
        for (int key : routinesCache.keySet()) {
            writeMessage(writer, routinesCache.get(key));
        }
        writer.endArray();
    }

    public static void writeMessage(JsonWriter writer, Routine routine) throws IOException {
        writer.beginObject();
        writer.name(ROUTINE_JSON_KEY_ID).value(routine.getId());
        writer.name(ROUTINE_JSON_KEY_NAME).value(routine.getName());
        writer.name(ROUTINE_JSON_KEY_ALARM).value(routine.getWakeupTime().getTimeInMillis());
        writer.name(ROUTINE_JSON_KEY_ENABLE).value(routine.getEnableRoutine());
        writer.name(ROUTINE_JSON_KEY_SHOWTASKS).value(routine.getShowTasks());
        writer.endObject();
    }

    private static void backupInternalFile(String fileName, Context context) throws IOException {
        FileOutputStream fileOutStream = null;
        FileInputStream fileInStream = null;
        FileChannel fileOutChannel = null;
        FileChannel fileInChannel = null;
        long bytesTransferred = 0;
        long bytesToTransfer = 0;
        boolean backupCorrupt = false;
        try {
            fileOutStream = context.openFileOutput(fileName + ".back", Context.MODE_PRIVATE);
            fileInStream = context.openFileInput(fileName);
            fileOutChannel = fileOutStream.getChannel();
            fileInChannel = fileInStream.getChannel();
            bytesToTransfer = fileInChannel.size();
            bytesTransferred = fileInChannel.transferTo(0, bytesToTransfer, fileOutChannel);
            if(bytesToTransfer != bytesTransferred){
                backupCorrupt = true;
            }
        }
        finally {
            try {
                if (fileOutStream != null) {
                    fileOutStream.close();
                }
                if (fileInStream != null) {
                    fileInStream.close();
                }
            }
            finally{
                if(backupCorrupt){
                    throw new IOException("Backup file "+fileName+".back is corrupt.");
                }
            }
        }
    }
}
