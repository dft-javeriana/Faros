package co.edu.javeriana.faros.tasks;

import android.net.Uri;

import java.io.File;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by cesar on 4/1/16.
 }*/

public class Task{
    private static AtomicInteger mAtomicInteger = null;
    private int mId = -1;
    private String mBleAddress = null;
    private String mName = null;
    private Uri mPicUri = null;

    public Task(String name, String bleAddress, Uri picUri) {
        if(mAtomicInteger == null){
            mAtomicInteger = new AtomicInteger();
        }
        mId = mAtomicInteger.getAndIncrement();
        mName = name;
        mBleAddress = bleAddress;
        mPicUri = picUri;
    }

    public Task(int id, String name, String bleAddress, Uri picUri) {
        if(mAtomicInteger == null){
            mAtomicInteger = new AtomicInteger();
        }
        mId = id;
        mName = name;
        mBleAddress = bleAddress;
        mPicUri = picUri;
    }

    public Task() {
        if(mAtomicInteger == null){
            mAtomicInteger = new AtomicInteger();
        }
    }

    public Task(Task another){
        mId = another.getId();
        mName = another.getName();
        mBleAddress = another.getBleAddress();
        mPicUri = another.getPicUri();
    }

    public int getId() {
        return mId;
    }

    public Uri getPicUri() {
        return mPicUri;
    }

    public String getName() {
        return mName;
    }

    public String getBleAddress() {
        return mBleAddress;
    }

    public void setId(int id) {
        mId = id;
    }

    public void setPicUriFromFile(File picFile) {
        if(picFile == null){
            mPicUri = null;
        }else {
            mPicUri = Uri.fromFile(picFile);
        }
    }

    public void setName(String name) {
        mName = name;
    }

    public void setBleAddress(String bleAddress) {

        mBleAddress = bleAddress;
    }

    public void newId(){
        mId = mAtomicInteger.getAndIncrement();
    }


}
