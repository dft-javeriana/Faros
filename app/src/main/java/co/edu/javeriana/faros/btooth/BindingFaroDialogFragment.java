package co.edu.javeriana.faros.btooth;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.edu.javeriana.faros.R;

/**
 * A simple {@link DialogFragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BindingFaroDialogFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BindingFaroDialogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BindingFaroDialogFragment extends DialogFragment {
    // the fragment initialization parameters
    private static final String ARG_ROUTINE_POS = "ARG_ROUTINE_POS";

    private String mRoutinePosition;

    private OnFragmentInteractionListener mListener;

    public BindingFaroDialogFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param routinePosition Position of routine to bind faro to.
     * @return A new instance of fragment BindingFaroDialogFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BindingFaroDialogFragment newInstance(String routinePosition) {
        BindingFaroDialogFragment fragment = new BindingFaroDialogFragment();
        Bundle args = new Bundle();
        args.putString(ARG_ROUTINE_POS, routinePosition);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mRoutinePosition = getArguments().getString(ARG_ROUTINE_POS);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_binding_faro_dialog, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
