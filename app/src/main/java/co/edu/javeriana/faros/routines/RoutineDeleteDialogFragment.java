package co.edu.javeriana.faros.routines;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import co.edu.javeriana.faros.R;

/**
 * A simple {@link DialogFragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RoutineDeleteDialogFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RoutineDeleteDialogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RoutineDeleteDialogFragment extends DialogFragment {
    // the fragment initialization parameters
    private static final String ARG_ROUTINE_NAME = "ARG_ROUTINE_NAME";
    private static final String ARG_ROUTINE_POS = "ARG_ROUTINE_POS";

    private String mRoutineName;
    private int mRoutinePosition;

    private OnFragmentInteractionListener mListener;

    public RoutineDeleteDialogFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param routinePosition Position of routine to be deleted.
     * @param routineName Name of routine to be deleted.
     * @return A new instance of fragment RoutineDeleteDialogFragment.
     */
    public static RoutineDeleteDialogFragment newInstance(int routinePosition, String routineName) {
        RoutineDeleteDialogFragment fragment = new RoutineDeleteDialogFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_ROUTINE_POS, routinePosition);
        args.putString(ARG_ROUTINE_NAME, routineName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mRoutineName = getArguments().getString(ARG_ROUTINE_NAME);
            mRoutinePosition = getArguments().getInt(ARG_ROUTINE_POS);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(((Context)mListener).getString(R.string.routine_delete_dialog_msg)+" "+mRoutineName+"?")
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.deleteRoutine(mRoutinePosition);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //nop
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks. */
    public interface OnFragmentInteractionListener {
        void deleteRoutine(int routinePosition);
    }
}
