package co.edu.javeriana.faros;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantLock;

import co.edu.javeriana.faros.btooth.BluetoothAdapterReceiver;
import co.edu.javeriana.faros.btooth.BluetoothFaroGattService;
import co.edu.javeriana.faros.btooth.BluetoothFarosScanner;
import co.edu.javeriana.faros.btooth.BluetoothRequest;
import co.edu.javeriana.faros.btooth.BluetoothState;
import co.edu.javeriana.faros.btooth.BluetoothFaroGattReceiver;
import co.edu.javeriana.faros.btooth.farosGattCharacteristics.SensorTagGattCharacteristics;
import co.edu.javeriana.faros.common.CallBack;
import co.edu.javeriana.faros.routines.Routine;
import co.edu.javeriana.faros.routines.RoutineDeleteDialogFragment;
import co.edu.javeriana.faros.routines.RoutinesManager;
import co.edu.javeriana.faros.routines.RoutinesRecyclerViewAdapter;

import co.edu.javeriana.faros.tasks.Task;

public class RoutinesWeekTabbedActivity extends AppCompatActivity
        implements ActionBar.TabListener, RoutineDeleteDialogFragment.OnFragmentInteractionListener{
    private static final String TAG = "RoutinesWTActivity";
    private static final int RESULT_CODE_ADD_ROUTINE = 0;
    private static final int RESULT_CODE_REQUEST_IMAGE_CAPTURE = 1;

    private static final short REQUEST_ACCESS_COARSE_LOCATION = 0;

    private static HashMap<String,Point3D> faroAccData = new HashMap<String,Point3D>();

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
     * will keep every loaded fragment in memory. If this becomes too memory
     * intensive, it may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    public SectionsPagerAdapter mSectionsPagerAdapter;

    private static BluetoothFaroGattService mBluetoothFaroGattService = null;
    private BroadcastReceiver mBluetoothFaroGattReceiver = null;

    private static CallBack<String, Integer> stopBindingFaroCB;
    private static CallBack<String, Integer> saveFaroCB;
    private static CallBack<String, Integer> deleteRoutineCB;
    /**
     * The {@link BluetoothAdapter} that performs scanning and contains
     * information of the bluetooth adapter's state.
     */
    private static BluetoothAdapter mBluetoothAdapter = null;

    private BroadcastReceiver mBluetoothAdapterReceiver = null;

    private TextView bluetoothStateTextView = null;
    private Button bluetoothEnableButton = null;

    public TextView getBluetoothStateTextView() {
        return bluetoothStateTextView;
    }

    public Button getBluetoothEnableButton() {
        return bluetoothEnableButton;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (android.os.Build.VERSION.SDK_INT >= 23) {
            ArrayList<String> permissionList = new ArrayList<String>();
            if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                permissionList.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            }
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                permissionList.add(Manifest.permission.CAMERA);
            }
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                permissionList.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                permissionList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            requestPermissions(permissionList.toArray(new String[permissionList.size()]), 0);
        }

        setContentView(R.layout.activity_routines_week_tabbed);

        final ActionBar actionBar = getSupportActionBar();

        // BEGIN_INCLUDE (setup_view_pager)
        // Create the adapter that will return a fragment for each of the three primary sections
        // of the app.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        // END_INCLUDE (setup_view_pager)

        // When swiping between different sections, select the corresponding tab. We can also use
        // ActionBar.Tab#select() to do this if we have a reference to the Tab.
        // BEGIN_INCLUDE (page_change_listener)
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
            }
        });
        // END_INCLUDE (page_change_listener)

        // BEGIN_INCLUDE (add_tabs)
        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
            // Create a tab with text corresponding to the page title defined by the adapter. Also
            // specify this Activity object, which implements the TabListener interface, as the
            // callback (listener) for when this tab is selected.
            actionBar.addTab(
                    actionBar.newTab()
                            .setText(mSectionsPagerAdapter.getPageTitle(i))
                            .setTabListener(this));
        }
        // END_INCLUDE (add_tabs)

        // Set up the action bar. The navigation mode is set to NAVIGATION_MODE_TABS, which will
        // cause the ActionBar to render a set of tabs. Note that these tabs are *not* rendered
        // by the ViewPager; additional logic is higher in this file to synchronize the ViewPager
        // state with the tab state. (See mViewPager.setOnPageChangeListener() and onTabSelected().)
        // BEGIN_INCLUDE (set_navigation_mode)
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        // END_INCLUDE (set_navigation_mode)

        //BEGIN_INCLUDE (init_ble_adapter)
        BluetoothManager mBluetoothManager = null;

        // Bluetooth enable button and state text view
        bluetoothStateTextView = (TextView) findViewById(R.id.bluetoothStateTextView);
        bluetoothEnableButton = (Button) findViewById(R.id.bluetoothEnableButton);

        // Initializes a Bluetooth adapter. For API level 18 and above, get a
        // reference to BluetoothAdapter through BluetoothManager.
        mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = mBluetoothManager.getAdapter();

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)
                || mBluetoothAdapter == null) {
            //TODO: replace with a permanent message box on top
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_LONG)
                    .show();
            BluetoothState.setBleSupport(false);
        }
        else{
            BluetoothState.setBleSupport(true);
        }

        if(BluetoothState.getBleSupport()) {
            // Initialize device filter
            Resources res = getResources();
            String[] deviceNameFilterArray = res.getStringArray(R.array.device_filter);
            // Set adapter and filter for bluetoothFarosScanner
            BluetoothFarosScanner.BluetoothScanner(mBluetoothAdapter,deviceNameFilterArray);
            if (android.os.Build.VERSION.SDK_INT >= 23) {
                if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                        Snackbar.make(bluetoothStateTextView, R.string.coarse_location_permission_rationale, Snackbar.LENGTH_INDEFINITE)
                                .setAction(android.R.string.ok, new View.OnClickListener() {
                                    @Override
                                    @TargetApi(Build.VERSION_CODES.M)
                                    public void onClick(View v) {
                                        requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_ACCESS_COARSE_LOCATION);
                                    }
                                });
                    } else {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_ACCESS_COARSE_LOCATION);
                    }
                }else{
                    BluetoothFarosScanner.scanInit();
                }
            }else{
                BluetoothFarosScanner.scanInit();
            }

            if (!mBluetoothAdapter.isEnabled()) {
                // Show message text box, show enable button
                bluetoothStateTextView.setVisibility(View.VISIBLE);
                bluetoothEnableButton.setVisibility(View.VISIBLE);
                bluetoothStateTextView.setText(R.string.bt_off);
                bluetoothStateTextView.setBackgroundColor(0xfff00000);
                bluetoothEnableButton.setText(R.string.bt_enable);
            }

            bluetoothEnableButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (!mBluetoothAdapter.isEnabled()) {
                        mBluetoothAdapter.enable();
                    }
                }
            });

            // Register for broadcasts on BluetoothAdapter state change
            if(mBluetoothAdapterReceiver == null){
                mBluetoothAdapterReceiver = new BluetoothAdapterReceiver();
            }
            IntentFilter bluetoothAdapterStateFilter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
            registerReceiver(mBluetoothAdapterReceiver, bluetoothAdapterStateFilter);

            if(mBluetoothFaroGattReceiver == null){
                mBluetoothFaroGattReceiver = new BluetoothFaroGattReceiver();
            }

            IntentFilter bluetoothFaroGattStateFilter = new IntentFilter();
            bluetoothFaroGattStateFilter.addAction(BluetoothFaroGattService.ACTION_GATT_CONNECTED);
            bluetoothFaroGattStateFilter.addAction(BluetoothFaroGattService.ACTION_GATT_DISCONNECTED);
            bluetoothFaroGattStateFilter.addAction(BluetoothFaroGattService.ACTION_GATT_SERVICES_DISCOVERED);
            bluetoothFaroGattStateFilter.addAction(BluetoothFaroGattService.ACTION_GATT_CHARACT_READ);
            bluetoothFaroGattStateFilter.addAction(BluetoothFaroGattService.ACTION_GATT_CHARACT_WRITE);
            bluetoothFaroGattStateFilter.addAction(BluetoothFaroGattService.ACTION_GATT_CHARACT_NOTIFY);
            bluetoothFaroGattStateFilter.addAction(BluetoothFaroGattService.ACTION_GATT_WRITE_COMMITTED);

            registerReceiver(mBluetoothFaroGattReceiver,bluetoothFaroGattStateFilter);

            Intent faroGattServiceIntent = new Intent(this, BluetoothFaroGattService.class);
            // Prevents destruction of service due to activity unbinding
            startService(faroGattServiceIntent);
            bindService(faroGattServiceIntent, BluetoothFaroGattServiceConnection,Context.BIND_AUTO_CREATE);

        } else {
            Log.e(TAG, "BLE not supported on this device");
        }
        //END_INCLUDE(init ble adapter)


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Unregister broadcast listener
        if(mBluetoothAdapterReceiver != null) {
            unregisterReceiver(mBluetoothAdapterReceiver);
        }
        if(mBluetoothFaroGattReceiver != null) {
            unregisterReceiver(mBluetoothFaroGattReceiver);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_ACCESS_COARSE_LOCATION) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                BluetoothFarosScanner.scanInit();
            }
        }
    }

    /**
     * Update {@link ViewPager} after a tab has been selected in the ActionBar.
     *
     * @param tab Tab that was selected.
     * @param fragmentTransaction A {@link android.app.FragmentTransaction} for queuing fragment operations to
     *                            execute once this method returns. This FragmentTransaction does
     *                            not support being added to the back stack.
     */
    // BEGIN_INCLUDE (on_tab_selected)
    @Override
    public void onTabSelected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction fragmentTransaction) {
        // When the given tab is selected, tell the ViewPager to switch to the corresponding page.
        mViewPager.setCurrentItem(tab.getPosition());
    }
    // END_INCLUDE (on_tab_selected)

    /**
     * Unused. Required for {@link android.app.ActionBar.TabListener}.
     */
    @Override
    public void onTabUnselected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction fragmentTransaction) {
    }

    /**
     * Unused. Required for {@link android.app.ActionBar.TabListener}.
     */
    @Override
    public void onTabReselected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        /**
         * super.onActivityResult Dispatches incoming result to the correct fragment.
         */
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void deleteRoutine(int routinePosition) {
        HashMap<String,Integer> data = new HashMap<String,Integer>();
        data.put(BluetoothFarosScanner.ARG_FARO_ROUTINE,routinePosition);
        deleteRoutineCB.callBackMethod(data);
    }

    private final ServiceConnection BluetoothFaroGattServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mBluetoothFaroGattService = ((BluetoothFaroGattService.serviceBinder) service).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBluetoothFaroGattService = null;
        }
    };

    public void faroGattConnected(String address, int status) {
        Toast.makeText(getApplicationContext(), "Connected to faro "+address, Toast.LENGTH_SHORT)
                .show();
        Log.d(TAG,"faroGattConnected "+address+" status:"+status);

        BluetoothRequest requestSearch = new BluetoothRequest()
                .setAction(BluetoothRequest.RequestAction.CONNECT)
                .setDevAddress(address)
                .setCharacteristic(BluetoothRequest.dummyChar);

        ReentrantLock requestSearchLock = new ReentrantLock();
        // Lock to prevent race condition on request search
        requestSearchLock.lock();
        BluetoothRequest request = BluetoothFaroGattService.reqsCache.get(requestSearch);
        if(request == null){
            Log.e(TAG,"faroGattConnected "+address+" request not found");
            return;
        }else{
            Log.d(TAG,"faroGattConnected "+address+" reqid " + request.getId());
        }
        if(!request.getStatus().equals(BluetoothRequest.RequestStatus.TO) &&
                !request.getStatus().equals(BluetoothRequest.RequestStatus.FAILED)) {
            // Unlock requests loop, we have an acknowledge
            BluetoothFaroGattService.reqAckSyncSemaphore.release();
        }
        request = BluetoothFaroGattService.reqsCache.remove(requestSearch);
        // Unlock mutex lock, search done
        requestSearchLock.unlock();

        Log.d(TAG, "faroGattConnected " + address + " reqid " + request.getId() + " reqStatus: " + request.getStatus().toString());
        if (status == BluetoothGatt.GATT_SUCCESS ) {
            mBluetoothFaroGattService.discoverFaroServices(address);
            Log.d(TAG, "faroGattConnected " + address + " reqid " + request.getId() + " discoverFaroServices");
        } else {
            mBluetoothFaroGattService.disconnectFaro(address);
            Log.d(TAG, "faroGattConnected " + address + " disconnect ");
        }
        // Unlock end of requests loop so that it may take a new request
        BluetoothFaroGattService.reqSyncSemaphore.release();
    }

    public void faroGattDisconnected(String address, int status) {
        Log.d(TAG,"faroGattDisconnected "+address+" status:"+status);
        //FIXME: if we try to disconnect, we don't receive the correct request
        BluetoothRequest requestSearch = new BluetoothRequest()
                .setAction(BluetoothRequest.RequestAction.DISCONNECT)
                .setDevAddress(address)
                .setCharacteristic(BluetoothRequest.dummyChar);

        ReentrantLock requestSearchLock = new ReentrantLock();
        // Lock to prevent race condition on request search
        requestSearchLock.lock();
        BluetoothRequest request = BluetoothFaroGattService.reqsCache.get(requestSearch);
        if(request == null){
            Log.e(TAG,"faroGattDisconnected "+address+" request not found");
            return;
        }else{
            Log.d(TAG,"faroGattDisconnected "+address+" reqid " + request.getId());
        }
        if(!request.getStatus().equals(BluetoothRequest.RequestStatus.TO) &&
                !request.getStatus().equals(BluetoothRequest.RequestStatus.FAILED)) {
            // Unlock requests loop, we have an acknowledge
            BluetoothFaroGattService.reqAckSyncSemaphore.release();
        }
        request = BluetoothFaroGattService.reqsCache.remove(requestSearch);
        // Unlock mutex lock, search done
        requestSearchLock.unlock();

        Log.d(TAG, "faroGattDisconnected "+ address+" reqid "+request.getId()+" reqStatus: "+request.getStatus().toString());

        // Unlock end of rquests loop so that it may take a new request
        BluetoothFaroGattService.reqSyncSemaphore.release();
    }

    private BluetoothGattCharacteristic movDataChar;
    public void faroGattServicesDiscovered(String address, int status) {
        Log.d(TAG,"faroGattServicesDiscovered "+address+" status:"+status);
        BluetoothRequest requestSearch = new BluetoothRequest()
                .setAction(BluetoothRequest.RequestAction.DISCOVER)
                .setDevAddress(address)
                .setCharacteristic(BluetoothRequest.dummyChar);

        ReentrantLock requestSearchLock = new ReentrantLock();
        // Lock to prevent race condition on request search
        requestSearchLock.lock();
        BluetoothRequest request = BluetoothFaroGattService.reqsCache.get(requestSearch);
        if(request == null){
            Log.e(TAG,"faroGattServicesDiscovered "+address+" request not found");
            return;
        }else{
            Log.d(TAG,"faroGattServicesDiscovered "+address+" reqid " + request.getId());
        }
        if(!request.getStatus().equals(BluetoothRequest.RequestStatus.TO) &&
                !request.getStatus().equals(BluetoothRequest.RequestStatus.FAILED)) {
            // Unlock requests loop, we have an acknowledge
            BluetoothFaroGattService.reqAckSyncSemaphore.release();
        }
        request = BluetoothFaroGattService.reqsCache.remove(requestSearch);
        // Unlock mutex lock, search done
        requestSearchLock.unlock();

        Log.d(TAG, "faroGattServicesDiscovered "+address+" reqid "+request.getId()+" reqStatus: "+request.getStatus().toString());

        if(status == BluetoothGatt.GATT_SUCCESS) {
            List<BluetoothGattService> faroServices = mBluetoothFaroGattService.getFaroServices(address);
            Log.d(TAG, "faroGattServicesDiscovered "+address+" reqid "+request.getId()+" found "+faroServices.size()+" services");
            for(BluetoothGattService faroService : faroServices){
                Log.d(TAG, "faroGattServicesDiscovered "+address+" reqid "+request.getId()+" service: "+faroService.getUuid());
                if(faroService.getUuid().equals(SensorTagGattCharacteristics.UUID_LIT_SERV)){
                    //turn on light
                    BluetoothGattCharacteristic litCompChar = faroService.getCharacteristic(SensorTagGattCharacteristics.UUID_LIT_COMP);
                    byte[] val = {0x00,(byte)0xFF,0x00,0x00};
                    litCompChar.setValue(val);
                    mBluetoothFaroGattService.writeCharacteristic(address, litCompChar);
                    Log.d(TAG, "faroGattServicesDiscovered "+address +" reqid "+request.getId()+litCompChar.getUuid()+" write char.");
                }
                if(faroService.getUuid().equals(SensorTagGattCharacteristics.UUID_MOV_SERV)){
                    // remove accelerometer data associated to this device address
                    // before we begin receiving new accelerometer data through mov notifications
                    faroAccData.remove(address);

                    //set mov service period
                    BluetoothGattCharacteristic movPeriChar = faroService.getCharacteristic(SensorTagGattCharacteristics.UUID_MOV_PERI);
                    byte[] val = {0x16}; // period is 250ms
                    movPeriChar.setValue(val);
                    mBluetoothFaroGattService.writeCharacteristic(address, movPeriChar);
                    Log.d(TAG, "faroGattServicesDiscovered "+ address +" reqid "+request.getId()+" "+movPeriChar.getUuid()+" write char.");

                    //enable mov service
                    BluetoothGattCharacteristic movConfChar = faroService.getCharacteristic(SensorTagGattCharacteristics.UUID_MOV_CONF);
                    val = new byte[] {0x38,0x02};
                    movConfChar.setValue(val);
                    mBluetoothFaroGattService.writeCharacteristic(address, movConfChar);
                    Log.d(TAG, "faroGattServicesDiscovered "+ address +" reqid "+request.getId()+" "+movConfChar.getUuid()+" write char.");

                    //enable mov notifications
                    movDataChar = faroService.getCharacteristic(SensorTagGattCharacteristics.UUID_MOV_DATA);
                    mBluetoothFaroGattService.setNotification(address,movDataChar,true);
                    Log.d(TAG, "faroGattServicesDiscovered "+ address +" reqid "+request.getId()+" "+movDataChar.getUuid()+" set noti. true");
                }
            }
        }
        else{
            Log.e(TAG, "faroGattServicesDiscovered failed "+address+" reqid "+request.getId()+" status "+status);
            mBluetoothFaroGattService.disconnectFaro(address);
            Log.d(TAG, "faroGattServicesDiscovered " + address + " disconnect ");
        }

        // Unlock end of requests loop so that it may take a new request
        BluetoothFaroGattService.reqSyncSemaphore.release();
    }

    public void faroGattCharacteristicRead(String address, int status, String uuid, byte[] value) {
        Log.d(TAG,"faroGattCharacteristicRead "+address+" "+uuid+" "+value.toString()+" status:"+status);
    }

    public void faroGattCharacteristicWrite(String address, int status, String uuid, byte[] value) {
        Log.d(TAG,"faroGattCharacteristicWrite "+address+" "+uuid+" "+value.toString()+" status:"+status);
        Log.d(TAG,"faroGattCharacteristicWrite "+address+" "+Integer.toHexString(value[0])+(value.length > 1?Integer.toHexString(value[1]):""));
        BluetoothRequest requestSearch = new BluetoothRequest()
                .setAction(BluetoothRequest.RequestAction.WRITE)
                .setDevAddress(address)
                .setCharacteristic(new BluetoothGattCharacteristic(UUID.fromString(uuid),0,0));

        ReentrantLock requestSearchLock = new ReentrantLock();
        // Mutex lock to prevent race condition on request search
        requestSearchLock.lock();
        BluetoothRequest request = BluetoothFaroGattService.reqsCache.get(requestSearch);
        if(request == null){
            Log.e(TAG,"faroGattCharacteristicWrite "+address+" "+uuid+" request not found");
            return;
        }else{
            Log.d(TAG,"faroGattCharacteristicWrite "+address+" "+uuid+" reqid " + request.getId());
        }
        if(!request.getStatus().equals(BluetoothRequest.RequestStatus.TO) &&
                !request.getStatus().equals(BluetoothRequest.RequestStatus.FAILED)) {
            // Unlock requests loop, we have an acknowledge
            BluetoothFaroGattService.reqAckSyncSemaphore.release();
        }
        request = BluetoothFaroGattService.reqsCache.remove(requestSearch);
        // Unlock mutex lock, search done
        requestSearchLock.unlock();

        if(status == BluetoothGatt.GATT_SUCCESS) {
            Log.d(TAG, "faroGattCharacteristicWrite " + address + " " + uuid + " reqid " + request.getId() + "reqStatus: " + request.getStatus().toString());
        }else {
            mBluetoothFaroGattService.disconnectFaro(address);
            Log.d(TAG, "faroGattCharacteristicWrite " + address + " disconnect ");
        }
        // Unlock end of requests loop so that it may take a new request
        BluetoothFaroGattService.reqSyncSemaphore.release();
    }

    public void faroGattCharacteristicNotify(String address, int status, String uuid, byte[] value, int routinePos) {
        Log.d(TAG,"faroGattCharacteristicNotify "+address+" "+uuid+" "+value.toString()+" status:"+status);

        Point3D newAccData = convert(value);
        Point3D oldAccData = faroAccData.get(address);
        faroAccData.put(address,newAccData);
        if(null != oldAccData){
            double distance = newAccData.distance(oldAccData);
            Log.d(TAG,"faroGattCharacteristicNotify "+address+" "+uuid+" distance:"+distance);
            if(distance > 4){
                Toast.makeText(getApplicationContext(), "Faro tapped", Toast.LENGTH_SHORT)
                        .show();
                faroAccData.remove(address);
                HashMap<String,Integer> data = new HashMap<String,Integer>();
                data.put(BluetoothFarosScanner.ARG_FARO_ROUTINE,routinePos);
                // Faro selected by user, it is saved to current routine on open activity
                saveFaroCB.callBackMethod(data);
                // Tell view adapter we have stopped binding.
                stopBindingFaroCB.callBackMethod(data);

                mBluetoothFaroGattService.setNotification(address,movDataChar,false);
                Log.d(TAG, "faroGattCharacteristicNotify " + address + " set not. false " + movDataChar.getUuid());
                mBluetoothFaroGattService.disconnectFaro(address);
                Log.d(TAG, "faroGattCharacteristicNotify " + address + " disconnect ");
            }
        }else{
            Toast.makeText(getApplicationContext(), "Waiting for tap on faro", Toast.LENGTH_SHORT)
                    .show();
        }
    }
    public void faroGattDescriptorWrite(String address, int status, String uuid, byte[] value) {
        Log.d(TAG,"faroGattDescriptorWrite "+address+" "+uuid+" "+value.toString()+" status:"+status);
        BluetoothRequest requestSearch = new BluetoothRequest()
                .setAction(BluetoothRequest.RequestAction.WRITE_DESCRIPTOR)
                .setDevAddress(address)
                .setCharacteristic(new BluetoothGattCharacteristic(UUID.fromString(uuid),0,0));

        ReentrantLock requestSearchLock = new ReentrantLock();
        // Lock to prevent race condition on request search
        requestSearchLock.lock();
        BluetoothRequest request = BluetoothFaroGattService.reqsCache.get(requestSearch);
        if(request == null){
            Log.e(TAG,"faroGattDescriptorWrite "+address+" "+uuid+" request not found");
            return;
        }else{
            Log.d(TAG,"faroGattDescriptorWrite "+address+" "+uuid+" reqid " + request.getId());
        }
        if(!request.getStatus().equals(BluetoothRequest.RequestStatus.TO) &&
                !request.getStatus().equals(BluetoothRequest.RequestStatus.FAILED)) {
            // Unlock requests loop, we have an acknowledge
            BluetoothFaroGattService.reqAckSyncSemaphore.release();
        }
        request = BluetoothFaroGattService.reqsCache.remove(requestSearch);
        // Unlock mutex lock, search done
        requestSearchLock.unlock();

        if(status == BluetoothGatt.GATT_SUCCESS) {
            Log.d(TAG, "faroGattDescriptorWrite " + address+" "+uuid+" reqid " + request.getId() + "reqStatus: " + request.getStatus().toString());
        }else {
            mBluetoothFaroGattService.disconnectFaro(address);
            Log.d(TAG, "faroGattDescriptorWrite " + address + " disconnect ");
        }
        BluetoothFaroGattService.reqSyncSemaphore.release();
    }

    public void faroGattWriteCommitted(String address, int status) {
        Log.d(TAG,"faroGattWriteCommitted "+address+" status:"+status);
    }


    public class Point3D {
        public double x, y, z;

        public Point3D(double x, double y, double z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public double distance(Point3D p){
            return Math.sqrt(Math.pow((this.x-p.x),2)+Math.pow((this.y-p.y),2)+Math.pow((this.z-p.z),2));
        }

    }

    public Point3D convert(final byte[] value) {
        // Range 8G
        final float SCALE = (float) 4096.0;

        int x = (value[7]<<8) + value[6];
        int y = (value[9]<<8) + value[8];
        int z = (value[11]<<8) + value[10];
        return new Point3D(((x / SCALE) * -1), y / SCALE, ((z / SCALE)*-1));
    }

    // BEGIN_INCLUDE (fragment_pager_adapter)
    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages. This provides the data for the {@link ViewPager}.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        // END_INCLUDE (fragment_pager_adapter)

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        // BEGIN_INCLUDE (fragment_pager_adapter_getitem)
        /**
         * Get fragment corresponding to a specific position. This will be used to populate the
         * contents of the {@link ViewPager}.
         *
         * @param position Position to fetch fragment for.
         * @return Fragment for specified position.
         */
        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a WeekSectionFragment (defined as a static inner class
            // below) with the page number as its lone argument.
            Fragment fragment = new WeekSectionFragment();
            Bundle args = new Bundle();
            args.putInt(WeekSectionFragment.ARG_SECTION_NUMBER, position + 1);
            fragment.setArguments(args);
            return fragment;
        }
        // END_INCLUDE (fragment_pager_adapter_getitem)

        // BEGIN_INCLUDE (fragment_pager_adapter_getcount)
        /**
         * Get number of pages the {@link ViewPager} should render.
         *
         * @return Number of fragments to be rendered as pages.
         */
        @Override
        public int getCount() {
            // Show 7 total pages.
            return 7;
        }
        // END_INCLUDE (fragment_pager_adapter_getcount)

        // BEGIN_INCLUDE (fragment_pager_adapter_getpagetitle)
        /**
         * Get title for each of the pages. This will be displayed on each of the tabs.
         *
         * @param position Page to fetch title for.
         * @return Title for specified page.
         */
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.week_sun_name);
                case 1:
                    return getString(R.string.week_mon_name);
                case 2:
                    return getString(R.string.week_tue_name);
                case 3:
                    return getString(R.string.week_wed_name);
                case 4:
                    return getString(R.string.week_thu_name);
                case 5:
                    return getString(R.string.week_fri_name);
                case 6:
                    return getString(R.string.week_sat_name);
            }
            return null;
        }
        // END_INCLUDE (fragment_pager_adapter_getpagetitle)
    }

    /**
     * A dummy fragment representing a section of the app, but that simply displays dummy text.
     * This would be replaced with your application's content.
     */
    public static class WeekSectionFragment extends Fragment {

        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        public static final String ARG_SECTION_NUMBER = "SEC_NUM";
        public static final String ARG_ROUTINE_POS = "ROUTINE_POS";
        public static final String ARG_TASK_POS = "TASK_POS";
        public static final String ARG_TASK_NAME = "TASK_NAME";

        private View rootView = null;

        private RecyclerView mRoutineRecyclerView = null;
        private RecyclerView.LayoutManager mRoutineLayoutManager = null;
        private RoutinesRecyclerViewAdapter mRoutinesAdapter = null;

        /**
         * photoFile contains the most recently created file.
         * photoIntentRoutinePos and photoIntentTaskPos, the position for
         * the routine and task that requested the photo intent.
         * None of these are threadsafe.
         */
        private File photoFile = null;
        private int photoIntentRoutinePos = -1;
        private int photoIntentTaskPos = -1;

        private CallBack<String, String> selectedFaroMACCB;

        public WeekSectionFragment() {
            //should be empty, parameters must be passed with setArguments/getArguments
        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            rootView = inflater.inflate(R.layout.fragment_routines_week, container, false);

            FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent mIntent = null;
                    mIntent = new Intent(getActivity(), AddRoutineActivity.class);
                    mIntent.putExtra(ARG_SECTION_NUMBER, getArguments().getInt(ARG_SECTION_NUMBER));
                    startActivityForResult(mIntent, RESULT_CODE_ADD_ROUTINE);
                }
            });

            // BEGIN_INCLUDE(initializeRecyclerView)
            mRoutineRecyclerView = (RecyclerView) rootView.findViewById(R.id.week_routines_recycler_view);

            int scrollPosition = 0;
            // If a layout manager has already been set, get current scroll position.
            if (mRoutineRecyclerView.getLayoutManager() != null) {
                scrollPosition = ((LinearLayoutManager) mRoutineRecyclerView.getLayoutManager())
                        .findFirstCompletelyVisibleItemPosition();
            }

            // RecyclerView.LayoutManager defines how elements are laid out. We lay them out in a linearly.
            mRoutineLayoutManager = new LinearLayoutManager(rootView.getContext());
            mRoutineRecyclerView.setLayoutManager(mRoutineLayoutManager);
            mRoutineRecyclerView.scrollToPosition(scrollPosition);

            // get routines list for view adapter
            RoutinesManager.readRoutinesFromDisk(rootView.getContext());
            int weekNumber = getArguments().getInt(ARG_SECTION_NUMBER);
            List<Routine> routinesList = RoutinesManager.getRoutinesListFromCacheWeekViewSorted(weekNumber);

            CallBack<String, Integer> requestImageCaptureCB = new CallBack<String, Integer>(){
                @Override
                public void callBackMethod(HashMap<? extends String, ? extends Integer> data) {
                    super.callBackMethod(data);

                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                        try {
                            photoFile = createImageFile("JPEG");
                        } catch (IOException e) {
                            e.printStackTrace();
                            return;
                        }
                        if(photoFile != null) {
                            Uri photoUri = Uri.fromFile(photoFile);
                            takePictureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                            takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            takePictureIntent.setClipData(ClipData.newRawUri(null, photoUri));
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                            photoIntentRoutinePos = data.get(ARG_ROUTINE_POS);
                            photoIntentTaskPos = data.get(ARG_TASK_POS);
                            startActivityForResult(takePictureIntent, RESULT_CODE_REQUEST_IMAGE_CAPTURE);
                        }
                    }
                }
            };

            CallBack<String, String> taskNameUpdateCB = new CallBack<String, String>(){
                @Override
                public void callBackMethod(HashMap<? extends String, ? extends String> data) {
                    super.callBackMethod(data);
                    int routinePosition = Integer.parseInt(data.get(ARG_ROUTINE_POS));
                    int taskPosition = Integer.parseInt(data.get(ARG_TASK_POS));
                    String taskName = data.get(ARG_TASK_NAME);
                    Task editTask = mRoutinesAdapter.getTaskItem(taskPosition,routinePosition);
                    if(editTask != null) {
                        editTask.setName(taskName);
                        // No need to alert task adapter to bind since the user is manipulating the
                        // edit text view. We expect binding to occur while scrolling.
                        mRoutinesAdapter.setTaskItem(editTask, taskPosition, routinePosition);
                    }
                }
            };

            CallBack<String, Integer> startBindingFaroCB = new CallBack<String, Integer>(){
                @Override
                public void callBackMethod(HashMap<? extends String, ? extends Integer> data) {
                    super.callBackMethod(data);
                    int routinePosition = data.get(ARG_ROUTINE_POS);
                    mRoutinesAdapter.setCurrentlyBinding(true);
                    scanForFaros(routinePosition);
                }
            };

            //TODO: we need to use this callback to allow user to cancel manually.
            stopBindingFaroCB = new CallBack<String, Integer>(){
                @Override
                public void callBackMethod(HashMap<? extends String, ? extends Integer> data){
                    super.callBackMethod(data);
                    mRoutinesAdapter.setCurrentlyBinding(false);
                }
            };

            selectedFaroMACCB = new CallBack<String, String>(){
                @Override
                public void callBackMethod(HashMap<? extends String, ? extends String> data) {
                    super.callBackMethod(data);
                    String selectedFaroMAC = data.get(BluetoothFarosScanner.ARG_FARO_MAC);
                    Log.d(TAG, "selectedFaroMACCB " + selectedFaroMAC);
                    int routinePosition = Integer.parseInt(data.get(BluetoothFarosScanner.ARG_FARO_ROUTINE));
                    int dummyTaskPosition = mRoutinesAdapter.getTaskItemCount(routinePosition)-1;
                    Task editTask = mRoutinesAdapter.getTaskItem(dummyTaskPosition, routinePosition);
                    if(editTask != null) {
                        editTask.setBleAddress(selectedFaroMAC);
                        mRoutinesAdapter.setTaskItem(editTask, dummyTaskPosition, routinePosition);
                        mRoutinesAdapter.notifyTaskItemChanged(dummyTaskPosition, routinePosition);
                    }
                    Log.d(TAG, "selectedFaroMACCB  " + selectedFaroMAC + " connectFaro");
                    Toast.makeText(rootView.getContext(), "Connecting to faro " + selectedFaroMAC, Toast.LENGTH_SHORT)
                            .show();
                    mBluetoothFaroGattService.connectFaro(
                            mBluetoothAdapter.getRemoteDevice(selectedFaroMAC),
                            routinePosition,
                            rootView.getContext());
                }
            };

            saveFaroCB = new CallBack<String, Integer>(){
                @Override
                public void callBackMethod(HashMap<? extends String, ? extends Integer> data){
                    super.callBackMethod(data);
                    int routinePosition = data.get(BluetoothFarosScanner.ARG_FARO_ROUTINE);
                    int dummyTaskPosition = mRoutinesAdapter.getTaskItemCount(routinePosition)-1;
                    Task dummyTask = mRoutinesAdapter.getTaskItem(dummyTaskPosition, routinePosition);
                    if(dummyTask != null) {
                        dummyTask.newId();
                        Task newTask = new Task(dummyTask);
                        mRoutinesAdapter.addTaskItem(newTask, routinePosition);
                        mRoutinesAdapter.resetTaskDummy(routinePosition);
                        mRoutinesAdapter.setCurrentlyBinding(false);
                        mRoutinesAdapter.notifyTaskItemChanged(dummyTaskPosition, routinePosition);
                        mRoutinesAdapter.notifyTaskItemInserted(dummyTaskPosition+1, routinePosition);
                    }
                }
            };

            deleteRoutineCB = new CallBack<String, Integer>(){
                public void callBackMethod(HashMap<? extends String, ? extends Integer> data) {
                    super.callBackMethod(data);
                    int routinePosition = data.get(BluetoothFarosScanner.ARG_FARO_ROUTINE);
                    int routineId = (int)mRoutinesAdapter.getItemId(routinePosition);
                    mRoutinesAdapter.deleteTasksForRoutineItem(routinePosition);
                    RoutinesManager.deleteRoutineFromCache(routineId);
                    try {
                        RoutinesManager.commitRoutinesToDisk(rootView.getContext());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    mRoutinesAdapter.deleteRoutineItem(routinePosition);
                    mRoutinesAdapter.notifyItemRemoved(routinePosition);
                }
            };

            CallBack[] taskCBs = {
                    requestImageCaptureCB,
                    taskNameUpdateCB,
                    startBindingFaroCB};

            mRoutinesAdapter = new RoutinesRecyclerViewAdapter(
                    rootView.getContext(),
                    routinesList,
                    taskCBs);
            // Set RoutinesRecyclerViewAdapter as the adapter for RecyclerView.
            mRoutineRecyclerView.setAdapter(mRoutinesAdapter);

            return rootView;
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            switch(requestCode){
                case RESULT_CODE_REQUEST_IMAGE_CAPTURE:
                    if (resultCode ==  Activity.RESULT_OK) {
                        int routinePosition = photoIntentRoutinePos;
                        int taskPosition = photoIntentTaskPos;
                        Task editTask = mRoutinesAdapter.getTaskItem(taskPosition, routinePosition);
                        if(editTask != null) {
                            editTask.setPicUriFromFile(photoFile);
                            mRoutinesAdapter.setTaskItem(editTask, taskPosition, routinePosition);
                            mRoutinesAdapter.notifyTaskItemChanged(taskPosition, routinePosition);
                        }
                    }
                    if (resultCode == Activity.RESULT_CANCELED) {
                        //Code if there's no result
                    }
                    break;
                case RESULT_CODE_ADD_ROUTINE:
                    if(resultCode == Activity.RESULT_OK){
                        String routineName = data.getStringExtra(AddRoutineActivity.ROUTINE_NAME_EXTRA);
                        boolean routineEnable = data.getBooleanExtra(AddRoutineActivity.ROUTINE_ENABLE_EXTRA, true);
                        boolean routineShowTasks = data.getBooleanExtra(AddRoutineActivity.ROUTINE_SHOW_TASKS_EXTRA, false);
                        int routineWakeupHour = data.getIntExtra(AddRoutineActivity.ROUTINE_WAKEUP_HOUR_EXTRA, 0);
                        int routineWakeupMinute = data.getIntExtra(AddRoutineActivity.ROUTINE_WAKEUP_MINUTE_EXTRA, 0);

                        Calendar routineWakeupTime = Calendar.getInstance();
                        routineWakeupTime.setFirstDayOfWeek(Calendar.SUNDAY);

                        int weekNumber = getArguments().getInt(ARG_SECTION_NUMBER);

                        long currentTimeMillis = routineWakeupTime.getTimeInMillis();
                        long dayDiff = weekNumber - routineWakeupTime.get(Calendar.DAY_OF_WEEK);
                        long hourDiff = routineWakeupHour - routineWakeupTime.get(Calendar.HOUR_OF_DAY);
                        long minuteDiff = routineWakeupMinute - routineWakeupTime.get(Calendar.MINUTE);

                        long changeInMillis = 0;

                        if(dayDiff < 0){
                            changeInMillis = RoutinesManager.WEEK_IN_MILLIS + dayDiff*RoutinesManager.DAY_IN_MILLIS;
                        }else if (dayDiff > 0){
                            changeInMillis = dayDiff*RoutinesManager.DAY_IN_MILLIS;
                        }
                        currentTimeMillis += changeInMillis;
                        routineWakeupTime.setTimeInMillis(currentTimeMillis);
                        changeInMillis = 0;

                        if(hourDiff != 0) {
                            if (dayDiff == 0 && hourDiff < 0) {
                                changeInMillis = RoutinesManager.WEEK_IN_MILLIS + hourDiff * RoutinesManager.HOUR_IN_MILLIS;
                            } else {
                                changeInMillis = hourDiff * RoutinesManager.HOUR_IN_MILLIS;
                            }
                        }
                        currentTimeMillis += changeInMillis;
                        routineWakeupTime.setTimeInMillis(currentTimeMillis);
                        changeInMillis = 0;

                        if(minuteDiff != 0) {
                            if (dayDiff == 0 && hourDiff == 0 && minuteDiff < 0) {
                                changeInMillis = RoutinesManager.WEEK_IN_MILLIS + minuteDiff * RoutinesManager.MINUTE_IN_MILLIS;
                            }else{
                                changeInMillis = minuteDiff * RoutinesManager.MINUTE_IN_MILLIS;
                            }
                        }
                        currentTimeMillis += changeInMillis;
                        routineWakeupTime.setTimeInMillis(currentTimeMillis);

                        Routine newRoutine = new Routine(routineName, routineWakeupTime, routineEnable, routineShowTasks);
                        RoutinesManager.addRoutineToCache(newRoutine);
                        try {
                            RoutinesManager.commitRoutinesToDisk(getActivity());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        int routinePosition = mRoutinesAdapter.addRoutineItem(newRoutine);
                        mRoutinesAdapter.notifyItemInserted(routinePosition);
                    }
                    if (resultCode == Activity.RESULT_CANCELED) {
                        //Write your code if there's no result
                    }
                    break;
                default:
                    break;
            }
        }
        @Override
        public void onPause(){
            super.onPause();

        }

        @Override
        public void onResume(){
            super.onResume();
        }

        private void scanForFaros(int routinePosition){
            if(BluetoothState.getBleSupport() && mBluetoothAdapter.isEnabled()){
                if (android.os.Build.VERSION.SDK_INT >= 23 &&
                        rootView.getContext().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                        Snackbar.make(rootView, R.string.coarse_location_permission_rationale, Snackbar.LENGTH_INDEFINITE)
                                .setAction(android.R.string.ok, new View.OnClickListener() {
                                    @Override
                                    @TargetApi(Build.VERSION_CODES.M)
                                    public void onClick(View v) {
                                        requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_ACCESS_COARSE_LOCATION);
                                    }
                                });
                    } else {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_ACCESS_COARSE_LOCATION);
                    }
                }else{
                    BluetoothFarosScanner.startScan(selectedFaroMACCB, routinePosition);
                }
            }
        }

        private File createImageFile(String fileType) throws IOException {
            String fileSuffix = null;
            if (fileType.equals("JPEG")) {
                fileSuffix = ".jpg";
            } else if (fileType.equals("BMP")) {
                fileSuffix = ".bmp";
            } else{
                return null;
            }

            // Create an image file name
            File storageDir = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES);
            String timeStamp = new  SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = fileType + "_" + timeStamp + "_";

            // TODO: see if this is useful to show image during routine initiation.
            // Save a file: path for use with ACTION_VIEW intents
            //mCurrentPhotoPath = "file:" + image.getAbsolutePath();
            return File.createTempFile(
                    imageFileName,  /* prefix */
                    fileSuffix,     /* suffix */
                    storageDir      /* directory */
            );
        }
    }
}
