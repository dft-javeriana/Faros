package co.edu.javeriana.faros.tasks;

import android.content.Context;
import android.net.Uri;
import android.util.JsonReader;
import android.util.JsonWriter;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by cesar on 4/5/16.
 *
 * Not thread safe.
 * If necessary, thread safety can be implemented with a semaphore on
 */
public class TasksManager {

    /**
     * JSON keys for the tasks
     */
    private static final String TASK_JSON_KEY_NAME = "name";
    private static final String TASK_JSON_KEY_ID = "id";
    private static final String TASK_JSON_KEY_BLEADDRESS = "bleAddress";
    private static final String TASK_JSON_KEY_PICURI = "picUri";

    private static final String TASKS_META_FILE = "tasks_meta";
    private HashMap<Integer,Task> tasksCache = new HashMap<Integer,Task>();
    private int mRoutineId = -1;

    public TasksManager(int routineId){
        mRoutineId = routineId;
    }

    /**
     * <p>Returns cached tasks (stored on memory) as a list .
     */
    public List<Task> getTasksListFromCache(){
        return new ArrayList<Task>(tasksCache.values());
    }

    /**
     * <p>Adds task to cached tasks map.
     */
    public void addTaskToCache(Task newTask) {
        tasksCache.put(newTask.getId(), newTask);
    }

    /**
     * <p>Deletes task from cache.
     */
    public void delTask(int id){
        tasksCache.remove(id);
    }

    /**
     * <p>Changes name for task on cache.
     */
    public void nameTask(int id, String name){
        Task task = null;
        task = tasksCache.get(id);
        task.setName(name);
        tasksCache.put(id, task);
    }


    /**
     * <p>Clears cache.
     */
    public void clearCache(){
        tasksCache.clear();
    }

    /**
     * <p>Clears cache and deletes tasks file on disk.
     */

    public void deleteTasksOnDisk(Context context){
        clearCache();
        context.deleteFile(TASKS_META_FILE + "_" + mRoutineId);
    }

    /**
     * <p>Commits changes from cache on disk.
     * <p>Creates a backup file in case there is a failure while writing to file.
     */

    public void commitTasksToDisk(Context context) throws IOException{
        OutputStream tasksMetaOutStream = null;
        String[] internalFilesList = null;
        internalFilesList = context.fileList();

       /* for(String internalFile : internalFilesList){
            if(internalFile.equals(ROUTINES_META_FILE+".back")){
                throw new IOException("A backup file already exists.");
            }
        }

        try{
            backupInternalFile (ROUTINES_META_FILE,context); //throws IOException
        } catch (IOException e){
            e.printStackTrace();
            if(!context.deleteFile(ROUTINES_META_FILE+".back")){
                throw new IOException("Could not delete backup file.");
            }
        }*/

        try {
            tasksMetaOutStream = new BufferedOutputStream(
                    context.openFileOutput(TASKS_META_FILE + "_" + mRoutineId, Context.MODE_PRIVATE));
        } catch (IOException e) {
            e.printStackTrace();
            try {
                if(tasksMetaOutStream != null) {
                    tasksMetaOutStream.close();
                }
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return;
        }

        try {
            writeTasksToJsonStream(tasksMetaOutStream);
        } catch (IOException e) {
            e.printStackTrace();
            throw new IOException("Could not write cache to disk. Attempted to restore from backup");
        }
/*
        if(!context.deleteFile(ROUTINES_META_FILE+".back")){
            throw new IOException("Could not delete backup file.");
        }
*/
    }

    /**
     * <p>Reads the list of tasks from disk and copies it to the tasks cache
     * that is on memory. If there are changes made to the tasks cache and
     * not committed, these will be reset to the values on last commit. Thus an
     * effort must be made to commit after changes are made and before the user
     * exits from the application or changes the context to maintain consistency.
     *
     * <p>Also, changes to related data must be made between changes made to
     * tasks on cache and the commit to minimize race conditions.
     *
     * @param context Context that will lock the file from which the
     *                        tasksList is read.
    */
    public void readTasksFromDisk(Context context){
        InputStream tasksMetaInStream = null;

        try {
            tasksMetaInStream = new BufferedInputStream(
                    context.openFileInput(TASKS_META_FILE + "_" + mRoutineId));
            readTasksFromJsonStream(tasksMetaInStream);
        } catch (IOException e) {
            e.printStackTrace();
            // There is no data in disk or cache may have been corrupted while reading.
            // Thus, we clear the cache.
            tasksCache.clear();
            if(tasksMetaInStream != null) {
                try {
                    tasksMetaInStream.close();
                }catch (IOException e1) {
                    e.printStackTrace();
                }
            }
            return;
        }

        try {
            if(tasksMetaInStream != null) {
                tasksMetaInStream.close();
            }
        }catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void readTasksFromJsonStream(InputStream in) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
        try {
            readTasksArray(reader);
        }
        finally{
            reader.close();
        }
    }

    private void readTasksArray(JsonReader reader) throws IOException {
        Task restoredTask;
        reader.beginArray();
        while (reader.hasNext()) {
            restoredTask = getJsonTask(reader);
            tasksCache.put(restoredTask.getId(), restoredTask);
        }
        reader.endArray();
    }

    private static Task getJsonTask(JsonReader reader) throws IOException {
        Task restoredTask = null;

        int id = -1;
        String taskName = null;
        String  bleAddress = null;
        Uri picUri = null;

        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals(TASK_JSON_KEY_ID)) {
                id = reader.nextInt();
            } else if (name.equals(TASK_JSON_KEY_NAME)) {
                taskName = reader.nextString();
            } else if (name.equals(TASK_JSON_KEY_BLEADDRESS)) {
                bleAddress = reader.nextString();
            } else if (name.equals(TASK_JSON_KEY_PICURI)) {
                String sPicUri = reader.nextString();
                if(sPicUri.equals("")){
                    picUri = null;
                }else {
                    picUri = Uri.parse(sPicUri);
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        restoredTask = new Task(id, taskName, bleAddress, picUri);
        return restoredTask;
    }

    public void writeTasksToJsonStream(OutputStream out) throws IOException {
        JsonWriter writer = new JsonWriter(new OutputStreamWriter(out, "UTF-8"));
        writer.setIndent("  ");
        try {
            writeMessagesArray(writer);
        }
        finally {
            writer.close();
        }
    }

    public void writeMessagesArray(JsonWriter writer) throws IOException {
        writer.beginArray();
        for (int key : tasksCache.keySet()) {
            writeMessage(writer, tasksCache.get(key));
        }
        writer.endArray();
    }

    public static void writeMessage(JsonWriter writer, Task task) throws IOException {
        writer.beginObject();
        writer.name(TASK_JSON_KEY_ID).value(task.getId());
        writer.name(TASK_JSON_KEY_NAME).value(task.getName());
        writer.name(TASK_JSON_KEY_BLEADDRESS).value(task.getBleAddress());
        if(task.getPicUri() != null) {
            writer.name(TASK_JSON_KEY_PICURI).value(task.getPicUri().toString());
        }else{
            writer.name(TASK_JSON_KEY_PICURI).value("");
        }
        writer.endObject();
    }

    private static void backupInternalFile(String fileName, Context context) throws IOException {
        FileOutputStream fileOutStream = null;
        FileInputStream fileInStream = null;
        FileChannel fileOutChannel = null;
        FileChannel fileInChannel = null;
        long bytesTransferred = 0;
        long bytesToTransfer = 0;
        boolean backupCorrupt = false;
        try {
            fileOutStream = context.openFileOutput(fileName + ".back", Context.MODE_PRIVATE);
            fileInStream = context.openFileInput(fileName);
            fileOutChannel = fileOutStream.getChannel();
            fileInChannel = fileInStream.getChannel();
            bytesToTransfer = fileInChannel.size();
            bytesTransferred = fileInChannel.transferTo(0, bytesToTransfer, fileOutChannel);
            if(bytesToTransfer != bytesTransferred){
                backupCorrupt = true;
            }
        }
        finally {
            try {
                if (fileOutStream != null) {
                    fileOutStream.close();
                }
                if (fileInStream != null) {
                    fileInStream.close();
                }
            }
            finally{
                if(backupCorrupt){
                    throw new IOException("Backup file "+fileName+".back is corrupt.");
                }
            }
        }
    }
}
