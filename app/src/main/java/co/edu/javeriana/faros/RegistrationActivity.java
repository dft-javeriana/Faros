package co.edu.javeriana.faros;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * A registration screen that offers registration via user/password.
 */
public class RegistrationActivity extends AppCompatActivity {

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;

    /**
     * An authentication store for the user's credentials
     */
    public static final String CREDENTIALS = "CredentialsStore";

    /**
     * Keep track of the registration task to ensure we can cancel it if requested.
     */
    private UserRegistrationTask mRegTask = null;

    // UI references.
    private EditText mPatientView;
    private EditText mDoctorsEmailView;
    private EditText mUserView;
    private EditText mConfirmPasswordView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mRegistrationFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        // Set up the registration form.
        mPatientView = (EditText) findViewById(R.id.patient);
        mDoctorsEmailView = (EditText) findViewById(R.id.doctors_email);
        mUserView = (EditText) findViewById(R.id.user);
        mPasswordView = (EditText) findViewById(R.id.password);
        mConfirmPasswordView = (EditText) findViewById(R.id.confirm_password);
        mConfirmPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.register || id == EditorInfo.IME_NULL) {
                    attemptRegistration();
                    return true;
                }
                return false;
            }
        });

        Button mUserRegistrationButton = (Button) findViewById(R.id.user_registration_button);
        if (mUserRegistrationButton != null) {
            mUserRegistrationButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    attemptRegistration();
                }
            });
        }

        mRegistrationFormView = findViewById(R.id.registration_form);
        mProgressView = findViewById(R.id.registration_progress);

    }

    /**
     * Attempts to register the account specified by the registration form.
     * If there are form errors (invalid user, missing fields, etc.), the
     * errors are presented and no actual registration attempt is made.
     */
    private void attemptRegistration() {
        if (mRegTask != null) {
            return;
        }

        // Reset errors.
        mPatientView.setError(null);
        mDoctorsEmailView.setError(null);
        mUserView.setError(null);
        mPasswordView.setError(null);
        mConfirmPasswordView.setError(null);

        // Store values at the time of the registration attempt.
        String patient = mPatientView.getText().toString();
        String doctorsEmail = mDoctorsEmailView.getText().toString();
        String user = mUserView.getText().toString();
        String password = mPasswordView.getText().toString();
        String passwordConfirmation = mConfirmPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid patient name.
        if (TextUtils.isEmpty(patient)) {
            mPatientView.setError(getString(R.string.error_field_required));
            focusView = mPatientView;
            cancel = true;
        }

        // Check for a valid email
        if(!isEmailValid(doctorsEmail)){
            mDoctorsEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mDoctorsEmailView;
            cancel = true;
        }

        // Check for a valid user name.
        if (TextUtils.isEmpty(user)) {
            mUserView.setError(getString(R.string.error_field_required));
            focusView = mUserView;
            cancel = true;
        }

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }else if(!passwordsMatch(password,passwordConfirmation)) {
            mConfirmPasswordView.setError(getString(R.string.error_passwords_dont_match));
            focusView = mConfirmPasswordView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt registration and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user registration attempt.
            showProgress(true);
            mRegTask = new UserRegistrationTask(patient,doctorsEmail,user,password);
            mRegTask.execute((Void) null);
        }
    }
    private boolean isUserValid(String user) {
        return user.length() > 4;
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    private boolean isEmailValid(String email){
        return email.contains("@");
    }

    private boolean passwordsMatch(String password1, String password2){
        return password1.equals(password2);
    }

    /**
     * Shows the progress UI and hides the registration form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mRegistrationFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mRegistrationFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mRegistrationFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mRegistrationFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Represents an asynchronous registration task used to authenticate
     * the user.
     */
    public class UserRegistrationTask extends AsyncTask<Void, Void, Boolean> {

        private final String mPatient;
        private final String mDoctorsEmail;
        private final String mUser;
        private final String mPassword;

        UserRegistrationTask(String patient, String doctorsEmail, String user, String password) {
            mPatient = patient;
            mDoctorsEmail = doctorsEmail;
            mUser = user;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            SharedPreferences credentialPrefs = getSharedPreferences(CREDENTIALS, Context.MODE_PRIVATE);
            SharedPreferences.Editor PrefsEditor = credentialPrefs.edit();
            PrefsEditor.putString("patient", mPatient);
            PrefsEditor.putString("doctor's email", mDoctorsEmail);
            PrefsEditor.putString("user", mUser);
            PrefsEditor.putString("password", mPassword);

            // Commit the edits!
            PrefsEditor.apply();

            // Exit with success
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            Intent mIntent;
            mRegTask = null;
            showProgress(false);

            if (success) {
                mIntent = new Intent(RegistrationActivity.this, RoutinesWeekTabbedActivity.class);
                startActivity(mIntent);
            }
            else{
                finish();
            }
        }

        @Override
        protected void onCancelled() {
            mRegTask = null;
            showProgress(false);
        }
    }
}

