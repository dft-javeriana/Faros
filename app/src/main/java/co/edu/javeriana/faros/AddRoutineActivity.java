package co.edu.javeriana.faros;

import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TimePicker;

import java.util.Calendar;

public class AddRoutineActivity extends AppCompatActivity {
    public static final String ROUTINE_NAME_EXTRA = "ROUTINE_NAME_EXTRA";
    public static final String ROUTINE_ENABLE_EXTRA = "ROUTINE_ENABLE_EXTRA";
    public static final String ROUTINE_SHOW_TASKS_EXTRA = "ROUTINE_SHOW_TASKS_EXTRA";
    public static final String ROUTINE_WAKEUP_HOUR_EXTRA = "ROUTINE_WAKEUP_HOUR_EXTRA";
    public static final String ROUTINE_WAKEUP_MINUTE_EXTRA = "ROUTINE_WAKEUP_MINUTE_EXTRA";


    private static int routineWakeupHour;
    private static int routineWakeupMinute;
    private static boolean routineEnable = true;
    private static boolean routineShowTasks = false;
    private static String routineName = new String("");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_routine);

        // Use the current time as the default value
        Calendar currentTime = Calendar.getInstance();
        routineWakeupHour = currentTime.get(Calendar.HOUR_OF_DAY);
        routineWakeupMinute = currentTime.get(Calendar.MINUTE);

        final EditText routineNameEditText=(EditText)findViewById(R.id.routineNameEditText);

        Switch enableSwitch = (Switch) findViewById(R.id.enableRoutineSwitch);

        if (enableSwitch != null) {
            enableSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    routineEnable = isChecked;
                }
            });
        }

        Button timePickButton=(Button)this.findViewById(R.id.timePickButton);
        if (timePickButton != null) {
            timePickButton.setText(routineWakeupHour + ":" + routineWakeupMinute);
            timePickButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    DialogFragment newFragment = new TimePickerFragment();
                    newFragment.show(getSupportFragmentManager(), "timePicker");
                }
            });
        }

        Button cancelButton=(Button)this.findViewById(R.id.cancelButton);
        if (cancelButton != null) {
            cancelButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent returnIntent = new Intent();
                    setResult(Activity.RESULT_CANCELED, returnIntent);
                    finish();
                }
            });
        }


        Button saveButton=(Button)this.findViewById(R.id.saveButton);
        if (saveButton != null) {
            saveButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if(routineNameEditText != null){
                        routineName = routineNameEditText.getText().toString();
                    }else{
                        routineName = "";
                    }
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra(ROUTINE_NAME_EXTRA,routineName);
                    returnIntent.putExtra(ROUTINE_ENABLE_EXTRA,routineEnable);
                    returnIntent.putExtra(ROUTINE_SHOW_TASKS_EXTRA,routineShowTasks);
                    returnIntent.putExtra(ROUTINE_WAKEUP_HOUR_EXTRA,routineWakeupHour);
                    returnIntent.putExtra(ROUTINE_WAKEUP_MINUTE_EXTRA,routineWakeupMinute);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                }
            });
        }

    }

    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        @NonNull
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            int hour = routineWakeupHour;
            int minute = routineWakeupMinute;

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            routineWakeupHour = hourOfDay;
            routineWakeupMinute = minute;
        }
    }
}
