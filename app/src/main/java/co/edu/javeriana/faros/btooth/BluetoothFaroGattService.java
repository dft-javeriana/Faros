package co.edu.javeriana.faros.btooth;

import android.app.Service;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import co.edu.javeriana.faros.btooth.farosGattCharacteristics.SensorTagGattCharacteristics;

public class BluetoothFaroGattService extends Service {
    private final static String TAG = BluetoothFaroGattService.class.getSimpleName();

    public final static String ACTION_GATT_CONNECTED = "co.edu.javeriana.faros.btooth.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED = "co.edu.javeriana.faros.btooth.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED = "co.edu.javeriana.faros.btooth.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_GATT_CHARACT_READ = "co.edu.javeriana.faros.btooth.ACTION_GATT_CHARACT_READ";
    public final static String ACTION_GATT_CHARACT_NOTIFY = "co.edu.javeriana.faros.btooth.ACTION_GATT_CHARACT_NOTIFY";
    public final static String ACTION_GATT_CHARACT_WRITE = "co.edu.javeriana.faros.btooth.ACTION_GATT_CHARACT_WRITE";
    public static final String ACTION_GATT_DESCRIPTOR_WRITE = "co.edu.javeriana.faros.btooth.ACTION_GATT_DESCRIPTOR_WRITE";
    public final static String ACTION_GATT_WRITE_COMMITTED = "co.edu.javeriana.faros.btooth.ACTION_GATT_WRITE_COMMITTED";

    public final static String EXTRA_DATA = "co.edu.javeriana.faros.btooth.EXTRA_DATA";
    public final static String EXTRA_UUID = "co.edu.javeriana.faros.btooth.EXTRA_UUID";
    public final static String EXTRA_STATUS = "co.edu.javeriana.faros.btooth.EXTRA_STATUS";
    public final static String EXTRA_ADDRESS = "co.edu.javeriana.faros.btooth.EXTRA_ADDRESS";
    public final static String EXTRA_ROUTINE_POS = "co.edu.javeriana.faros.btooth.EXTRA_ROUTINE_POS";

    //public final static int GATT_REQUEST_NOT_INITIATED = -1;
    public final static int GATT_REQUEST_TIMEOUT = -2;
    public final static int GATT_REQUEST_FAILED = -3;

    private final static short TIMEOUT_WAITING_FOR_ACK = 0;
    private final static short EXCEPTION_WAITING_FOR_ACK = 1;
    private final static short SUCCESS_WAITING_FOR_ACK = 2;

    private static HashMap<String,BluetoothGatt> faroGattCommHashMap = new HashMap<String,BluetoothGatt>();
    private static HashMap<String,Boolean> faroGattBusy = new HashMap<String,Boolean>();
    private static HashMap<String,Integer> faroRoutinePosHashMap = new HashMap<String,Integer>();

    private static LinkedBlockingQueue<BluetoothRequest> reqQueue = new LinkedBlockingQueue<BluetoothRequest>();
    public static Semaphore reqAckSyncSemaphore = new Semaphore(0);
    public static Semaphore reqSyncSemaphore = new Semaphore(0);

    public static TreeMap<BluetoothRequest,BluetoothRequest> reqsCache = new TreeMap<BluetoothRequest,BluetoothRequest>();

    @Override
    public void onCreate() {
        Thread qRequestConsumer = new Thread() {
            @Override
            public void run() {
                Log.d(TAG,"qRequestConsumer thread id " + getId());
                //noinspection InfiniteLoopStatement
                for (; ; ) {
                    BluetoothRequest currReq;
                    try {
                        currReq = reqQueue.take();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        continue;
                    }
                    short reqTries = 0;
                    cacheReqStatus(currReq,BluetoothRequest.RequestStatus.STARTING,reqTries);
                    BluetoothGatt faroGattComm = faroGattCommHashMap.get(currReq.getDevAddress());
                    switch (currReq.getAction()) {
                        case NULL:
                            // No action
                            break;
                        case CONNECT:
                            while (++reqTries <= 3) {
                                if(faroGattComm == null){
                                    // Connects to Gatt server, we don't autoconnect
                                    faroGattComm = currReq.getDevice().connectGatt(currReq.getContext(),false,mBluetoothGattCallback);
                                    faroGattCommHashMap.put(currReq.getDevAddress(),faroGattComm);
                                    //TODO: we remove faro from this map when activity is deleted.
                                    faroRoutinePosHashMap.put(currReq.getDevAddress(),currReq.getRoutinePosition());
                                }else{
                                    // We reconnect to device if we already have a BluetoothGatt.
                                    if(!faroGattComm.connect()){
                                        cacheReqStatus(currReq,BluetoothRequest.RequestStatus.FAILED,reqTries);
                                        Log.e(TAG,"qRequestConsumer reconnect failed "+ currReq.getDevAddress()
                                                + " " + currReq.getAction().toString() + " try "+ reqTries);
//                                        mBluetoothGattCallback.onConnectionStateChange(
//                                                faroGattCommHashMap.get(currReq.getDevAddress()),
//                                                GATT_REQUEST_NOT_INITIATED,BluetoothProfile.STATE_DISCONNECTED);
                                        continue;
                                    }
                                }
                                cacheReqStatus(currReq,BluetoothRequest.RequestStatus.STARTED,reqTries);
                                short waitRes = waitForAck();
                                if( waitRes == TIMEOUT_WAITING_FOR_ACK) {
                                    cacheReqStatus(currReq,BluetoothRequest.RequestStatus.TO,reqTries);
                                }else if(waitRes == EXCEPTION_WAITING_FOR_ACK) {
                                    cacheReqStatus(currReq,BluetoothRequest.RequestStatus.FAILED,reqTries);
                                    reqTries = 3;
                                }else if (waitRes == SUCCESS_WAITING_FOR_ACK) {
                                    cacheReqStatus(currReq,BluetoothRequest.RequestStatus.DONE,reqTries);
                                    break;
                                }
                            }
                            if (currReq.getStatus().equals(BluetoothRequest.RequestStatus.TO)) {
                                mBluetoothGattCallback.onConnectionStateChange(
                                        faroGattCommHashMap.get(currReq.getDevAddress()),
                                        GATT_REQUEST_TIMEOUT, BluetoothProfile.STATE_DISCONNECTED);
                            }else if(currReq.getStatus().equals(BluetoothRequest.RequestStatus.FAILED)){
                                mBluetoothGattCallback.onConnectionStateChange(
                                        faroGattCommHashMap.get(currReq.getDevAddress()),
                                        GATT_REQUEST_FAILED, BluetoothProfile.STATE_DISCONNECTED);
                            }
                            break;
                        case DISCONNECT:
                            reqTries = 0;
                            while (++reqTries <= 3) {
                                faroGattComm.disconnect();
                                cacheReqStatus(currReq,BluetoothRequest.RequestStatus.STARTED,reqTries);
                                short waitRes = waitForAck();
                                if( waitRes == TIMEOUT_WAITING_FOR_ACK) {
                                    cacheReqStatus(currReq,BluetoothRequest.RequestStatus.TO,reqTries);
                                }else if(waitRes == EXCEPTION_WAITING_FOR_ACK) {
                                    cacheReqStatus(currReq,BluetoothRequest.RequestStatus.FAILED,reqTries);
                                    reqTries = 3;
                                }else if (waitRes == SUCCESS_WAITING_FOR_ACK) {
                                    cacheReqStatus(currReq,BluetoothRequest.RequestStatus.DONE,reqTries);
                                    break;
                                }
                            }
                            if (currReq.getStatus().equals(BluetoothRequest.RequestStatus.TO)) {
                                mBluetoothGattCallback.onConnectionStateChange(
                                        faroGattCommHashMap.get(currReq.getDevAddress()),
                                        GATT_REQUEST_TIMEOUT, BluetoothProfile.STATE_DISCONNECTED);
                            }else if(currReq.getStatus().equals(BluetoothRequest.RequestStatus.FAILED)){
                                mBluetoothGattCallback.onConnectionStateChange(
                                        faroGattCommHashMap.get(currReq.getDevAddress()),
                                        GATT_REQUEST_FAILED, BluetoothProfile.STATE_DISCONNECTED);
                            }
                            break;
                        case DISCOVER:
                            reqTries = 0;
                            while (++reqTries <= 3) {
                                if (faroGattComm.discoverServices()) {
                                    cacheReqStatus(currReq,BluetoothRequest.RequestStatus.STARTED,reqTries);
                                    short waitRes = waitForAck();
                                    if( waitRes == TIMEOUT_WAITING_FOR_ACK) {
                                        cacheReqStatus(currReq,BluetoothRequest.RequestStatus.TO,reqTries);
                                    }else if(waitRes == EXCEPTION_WAITING_FOR_ACK) {
                                        cacheReqStatus(currReq,BluetoothRequest.RequestStatus.FAILED,reqTries);
                                        reqTries = 3;
                                    }else if (waitRes == SUCCESS_WAITING_FOR_ACK) {
                                        cacheReqStatus(currReq,BluetoothRequest.RequestStatus.DONE,reqTries);
                                        break;
                                    }
                                } else {
                                    cacheReqStatus(currReq,BluetoothRequest.RequestStatus.FAILED,reqTries);
//                                    mBluetoothGattCallback.onCharacteristicWrite(
//                                            faroGattCommHashMap.get(currReq.getDevAddress()),
//                                            currReq.getCharacteristic(), GATT_REQUEST_NOT_INITIATED);
                                    continue;
                                }
                            }
                            if (currReq.getStatus().equals(BluetoothRequest.RequestStatus.TO)) {
                                mBluetoothGattCallback.onServicesDiscovered(
                                        faroGattCommHashMap.get(currReq.getDevAddress()), GATT_REQUEST_TIMEOUT);
                            }else if(currReq.getStatus().equals(BluetoothRequest.RequestStatus.FAILED)){
                                mBluetoothGattCallback.onServicesDiscovered(
                                        faroGattCommHashMap.get(currReq.getDevAddress()), GATT_REQUEST_FAILED);
                            }
                            break;
                        case GET_SERVICES:
                            break;
                        case READ:
                            break;
                        case WRITE:
                            reqTries = 0;
                            while (++reqTries <= 3) {
                                if (faroGattComm.writeCharacteristic(currReq.getCharacteristic())) {
                                    cacheReqStatus(currReq,BluetoothRequest.RequestStatus.STARTED,reqTries);
                                    short waitRes = waitForAck();
                                    if( waitRes == TIMEOUT_WAITING_FOR_ACK) {
                                        cacheReqStatus(currReq,BluetoothRequest.RequestStatus.TO,reqTries);
                                    }else if(waitRes == EXCEPTION_WAITING_FOR_ACK) {
                                        cacheReqStatus(currReq,BluetoothRequest.RequestStatus.FAILED,reqTries);
                                        reqTries = 3;
                                    }else if (waitRes == SUCCESS_WAITING_FOR_ACK) {
                                        cacheReqStatus(currReq,BluetoothRequest.RequestStatus.DONE,reqTries);
                                        break;
                                    }
                                } else {
                                    cacheReqStatus(currReq,BluetoothRequest.RequestStatus.FAILED,reqTries);
//                                    mBluetoothGattCallback.onCharacteristicWrite(
//                                            faroGattCommHashMap.get(currReq.getDevAddress()),
//                                            currReq.getCharacteristic(), GATT_REQUEST_NOT_INITIATED);
                                    continue;
                                }
                            }
                            if (currReq.getStatus().equals(BluetoothRequest.RequestStatus.TO)) {
                                mBluetoothGattCallback.onCharacteristicWrite(
                                        faroGattCommHashMap.get(currReq.getDevAddress()),
                                        currReq.getCharacteristic(), GATT_REQUEST_TIMEOUT);
                            }else if(currReq.getStatus().equals(BluetoothRequest.RequestStatus.FAILED)){
                                mBluetoothGattCallback.onCharacteristicWrite(
                                        faroGattCommHashMap.get(currReq.getDevAddress()),
                                        currReq.getCharacteristic(), GATT_REQUEST_FAILED);
                            }
                            break;
                        case SET_NOTIFICATION:
                            // Does not wait on ack
                            reqTries = 0;
                            while (++reqTries <= 3) {
                                if(faroGattComm.setCharacteristicNotification(currReq.getCharacteristic(), currReq.isNotificationEnabled())) {
                                    BluetoothGattDescriptor faroGattDesc =
                                            currReq.getCharacteristic().getDescriptor(SensorTagGattCharacteristics.CLIENT_CHARACTERISTIC_CONFIG);
                                    if(!faroGattDesc.setValue(
                                            currReq.isNotificationEnabled()?
                                                    BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE:
                                                    BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE)){
                                        cacheReqStatus(currReq,BluetoothRequest.RequestStatus.FAILED,reqTries);
                                        continue;
                                    }else{
                                        if(faroGattComm.writeDescriptor(faroGattDesc)) {
                                            cacheReqStatus(currReq,BluetoothRequest.RequestStatus.STARTED,reqTries);
//                                            short waitRes = waitForAck();
//                                            if (waitRes == TIMEOUT_WAITING_FOR_ACK) {
//                                                cacheReqStatus(currReq,BluetoothRequest.RequestStatus.TO,reqTries);
//                                            } else if (waitRes == EXCEPTION_WAITING_FOR_ACK) {
//                                                cacheReqStatus(currReq,BluetoothRequest.RequestStatus.FAILED,reqTries);
//                                                reqTries = 3;
//                                            } else if (waitRes == SUCCESS_WAITING_FOR_ACK) {
//                                                cacheReqStatus(currReq,BluetoothRequest.RequestStatus.DONE,reqTries);
//                                                break;
//                                            }
                                        }
                                        else{
                                            cacheReqStatus(currReq,BluetoothRequest.RequestStatus.FAILED,reqTries);
                                            continue;
                                        }
                                    }
                                }else{
                                    cacheReqStatus(currReq,BluetoothRequest.RequestStatus.FAILED,reqTries);
                                    continue;
                                }
                                cacheReqStatus(currReq,BluetoothRequest.RequestStatus.DONE,reqTries);
                                break;
                            }
                            break;
                    }
                    if(!currReq.isAsync()) {
                        // We lock here until callback is done
                        reqSyncSemaphore.acquireUninterruptibly();
                    }
                    try {
                        // 0.1 ms sleep, allows callback to continue, delays next request
                        sleep(0, 100000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        qRequestConsumer.start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        for(BluetoothGatt faroGattComm : faroGattCommHashMap.values()){
            faroGattComm.close();
        }
    }
    @Override
    public IBinder onBind(Intent intent) {
        return mIBinder;
    }

    @Override
    public boolean onUnbind(Intent intent){
        return super.onUnbind(intent);
    }

    public class serviceBinder extends Binder{
        public BluetoothFaroGattService getService(){
            return BluetoothFaroGattService.this;
        }
    }

    private final IBinder mIBinder = new serviceBinder();

    public short waitForAck(){
        boolean exception = false;
        boolean lockRes = false;
        short result = -1;
        try {
            // We lock until callback is called or we timeout
            lockRes = reqAckSyncSemaphore.tryAcquire(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            exception = true;
            e.printStackTrace();
            result = EXCEPTION_WAITING_FOR_ACK;
        } finally {
            if(!lockRes) {
                result = TIMEOUT_WAITING_FOR_ACK;
            }
        }
        if (!exception && lockRes) {
            // Release lock so that callback may lock it again and exit
            result = SUCCESS_WAITING_FOR_ACK;
        }
        return result;
    }

    private void cacheReqStatus(BluetoothRequest req,
                                BluetoothRequest.RequestStatus status,
                                short reqTries){
        req.setStatus(status);
        reqsCache.put(req, req);
        Log.d(TAG,"qRequestConsumer "+req.getDevAddress()+" "+req.getAction().toString()
                +" reqid "+req.getId()+" "+status.toString()+" "+" try "+reqTries);
    }

    public void connectFaro(BluetoothDevice faro, int routinePosition, Context context) {
        String address = faro.getAddress();
        try {
            reqQueue.put(new BluetoothRequest()
                    .setAsync(false)
                    .setAction(BluetoothRequest.RequestAction.CONNECT)
                    .setCharacteristic(BluetoothRequest.dummyChar)
                    .setDevAddress(address)
                    .setDevice(faro)
                    .setStatus(BluetoothRequest.RequestStatus.ON_QUEUE)
                    .setContext(context)
                    .setRoutinePosition(routinePosition)
            );
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void disconnectFaro(String address){
        try {
            reqQueue.put(new BluetoothRequest()
                    .setAsync(false)
                    .setAction(BluetoothRequest.RequestAction.DISCONNECT)
                    .setCharacteristic(BluetoothRequest.dummyChar)
                    .setDevAddress(address)
                    .setStatus(BluetoothRequest.RequestStatus.ON_QUEUE)
                    //.setRoutinePosition(routinePosition)
            );
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void discoverFaroServices(String address){
        try {
            reqQueue.put(new BluetoothRequest()
                    .setAsync(false)
                    .setAction(BluetoothRequest.RequestAction.DISCOVER)
                    .setCharacteristic(BluetoothRequest.dummyChar)
                    .setDevAddress(address)
                    .setStatus(BluetoothRequest.RequestStatus.ON_QUEUE)
            );
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public List<BluetoothGattService> getFaroServices(String address){
        BluetoothGatt faroGattComm = faroGattCommHashMap.get(address);
        return faroGattComm.getServices();
    }

    public void setNotification(String address, BluetoothGattCharacteristic gattCharact, boolean enable) {
        try {
            reqQueue.put(new BluetoothRequest()
                    .setAsync(true)
                    .setAction(BluetoothRequest.RequestAction.SET_NOTIFICATION)
                    .setCharacteristic(gattCharact)
                    .setNotificationEnabled(enable)
                    .setDevAddress(address)
                    .setStatus(BluetoothRequest.RequestStatus.ON_QUEUE)
            );
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public boolean readCharacteristic(String address, BluetoothGattCharacteristic gattCharact){
        BluetoothGatt faroGattComm = faroGattCommHashMap.get(address);
        return faroGattComm.readCharacteristic(gattCharact);
    }

    public static void writeCharacteristic(String address, BluetoothGattCharacteristic gattCharact){
        try {
            reqQueue.put(new BluetoothRequest()
                    .setAsync(false)
                    .setAction(BluetoothRequest.RequestAction.WRITE)
                    .setCharacteristic(gattCharact)
                    .setDevAddress(address)
                    .setStatus(BluetoothRequest.RequestStatus.ON_QUEUE)
            );
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    
    public boolean beginReliableWrite(String address){
        BluetoothGatt faroGattComm = faroGattCommHashMap.get(address);
        return faroGattComm.beginReliableWrite();
    }

    public boolean executeReliableWrite(String address){
        BluetoothGatt faroGattComm = faroGattCommHashMap.get(address);
        return faroGattComm.executeReliableWrite();
    }

    public void abortReliableWrite(String address, BluetoothDevice faro){
        BluetoothGatt faroGattComm = faroGattCommHashMap.get(address);
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            faroGattComm.abortReliableWrite();
        }else{
            faroGattComm.abortReliableWrite(faro);
        }
    }

    private final BluetoothGattCallback mBluetoothGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            String address = gatt.getDevice().getAddress();
            switch (newState) {
                case BluetoothProfile.STATE_CONNECTED:
                    broadcastUpdate(ACTION_GATT_CONNECTED, address, status);
                    break;
                case BluetoothProfile.STATE_DISCONNECTED:
                    broadcastUpdate(ACTION_GATT_DISCONNECTED, address, status);
                    break;
                default:
                    Log.e(TAG, "onConnectionStateChange new state not processed: " + newState);
                    break;
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED, gatt.getDevice().getAddress(), status);
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            broadcastUpdate(ACTION_GATT_CHARACT_READ, gatt.getDevice().getAddress(), status, characteristic);
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            broadcastUpdate(ACTION_GATT_CHARACT_WRITE, gatt.getDevice().getAddress(), status, characteristic);
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            int routinePosition = faroRoutinePosHashMap.get(gatt.getDevice().getAddress());
            broadcastUpdate(ACTION_GATT_CHARACT_NOTIFY, gatt.getDevice().getAddress(), BluetoothGatt.GATT_SUCCESS, characteristic,routinePosition);
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            broadcastUpdate(ACTION_GATT_DESCRIPTOR_WRITE, gatt.getDevice().getAddress(), status, descriptor);
        }

        @Override
        public void onReliableWriteCompleted(BluetoothGatt gatt, int status) {
            broadcastUpdate(ACTION_GATT_WRITE_COMMITTED, gatt.getDevice().getAddress(), status);
        }
    };


    private void broadcastUpdate(final String action, final String address, final int status) {
        final Intent intent = new Intent(action);
        intent.putExtra(EXTRA_ADDRESS, address);
        intent.putExtra(EXTRA_STATUS, status);
        sendBroadcast(intent);
    }

    private void broadcastUpdate(final String action, final String address, final int status, 
                                 final BluetoothGattCharacteristic characteristic) {
        final Intent intent = new Intent(action);
        intent.putExtra(EXTRA_ADDRESS, address);
        intent.putExtra(EXTRA_STATUS, status);
        intent.putExtra(EXTRA_UUID, characteristic.getUuid().toString());
        intent.putExtra(EXTRA_DATA, characteristic.getValue());
        sendBroadcast(intent);
    }
    private void broadcastUpdate(final String action, final String address, final int status, final BluetoothGattDescriptor descriptor) {
        final Intent intent = new Intent(action);
        intent.putExtra(EXTRA_ADDRESS, address);
        intent.putExtra(EXTRA_STATUS, status);
        intent.putExtra(EXTRA_UUID, descriptor.getUuid().toString());
        intent.putExtra(EXTRA_DATA, descriptor.getValue());
        sendBroadcast(intent);
    }

    private void broadcastUpdate(final String action, final String address, final int status,
                                 final BluetoothGattCharacteristic characteristic, final int routinePos) {
        final Intent intent = new Intent(action);
        intent.putExtra(EXTRA_ADDRESS, address);
        intent.putExtra(EXTRA_STATUS, status);
        intent.putExtra(EXTRA_UUID, characteristic.getUuid().toString());
        intent.putExtra(EXTRA_DATA, characteristic.getValue());
        intent.putExtra(EXTRA_ROUTINE_POS, routinePos);
        sendBroadcast(intent);
    }
}
